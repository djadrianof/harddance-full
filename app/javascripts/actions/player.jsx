import { Link, browserHistory } from 'react-router';

// ------------------------------------------------------
// Define Actions
// ------------------------------------------------------

export function open( video ) {
  return { type: 'PLAYER_OPEN', video };
}

export function close() {
  return (dispatch, getState) => {
    browserHistory.push(`/`);
    dispatch({ type: 'PLAYER_CLOSE' });
  }
}
