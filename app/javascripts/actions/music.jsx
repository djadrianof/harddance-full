import axios from 'axios';
import { playerActions } from './index';

// ------------------------------------------------------
// Define Actions
// ------------------------------------------------------

export function loading() {
  return { type: 'MUSIC_LOADING' };
}

export function loadedMusic( musics ) {
  return { type: 'MUSIC_LOADED', musics };
}

export function loadedMoreMusic( musics, page ) {
  return { type: 'MUSIC_MORE_LOADED', musics, page };
}

// ------------------------------------------------------
// Below example of No Pure Actions
// ------------------------------------------------------

export function getMusicVideo( videoId ) {
  return (dispatch, getState) => {
    axios
      .get(`/api/videos/${ videoId }`)
      .then((response) => {
        let videoProps = {
          id: response.data.videoId,
          title: response.data.videoTitle
        };

        dispatch(playerActions.open( videoProps ));
      })
  }
}
