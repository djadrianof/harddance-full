import axios from 'axios';
import { musicActions, podcastActions } from './index';

// ------------------------------------------------------
// Define Actions
// ------------------------------------------------------

export function loading() {
  return { type: 'HOME_LOADING' };
}

export function loadedContent() {
  return { type: 'HOME_LOADED' };
}

// ------------------------------------------------------
// Below example of No Pure Actions
// ------------------------------------------------------

export function getContent() {
  return (dispatch, getState) => {
    axios
      .get('/api/home')
      .then((response) => {
        dispatch(musicActions.loadedMusic( response.data.music ));
        dispatch(podcastActions.loadedPodcast( response.data.podcasts.data ));
        dispatch({ type: 'HOME_LOADED' });
      });
  }
}

export function getMoreContent( page ) {
  return (dispatch, getState) => {
    let currentMusics = getState().music.musics;

    dispatch(musicActions.loading());

    axios
      .get(`/api/home/${ page }`)
      .then((response) => {
        currentMusics = currentMusics.concat( response.data.music );
        dispatch(musicActions.loadedMoreMusic( currentMusics, page ));
      })
  }
}
