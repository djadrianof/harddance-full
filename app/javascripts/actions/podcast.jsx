// ------------------------------------------------------
// Define Actions
// ------------------------------------------------------

export function loading() {
  return { type: 'PODCAST_LOADING' };
}

export function loadedPodcast( podcasts ) {
  return { type: 'PODCAST_LOADED', podcasts };
}

// ------------------------------------------------------
// Below example of No Pure Actions
// ------------------------------------------------------

export function getMusic() {
  return (dispatch, getState) => {}
}
