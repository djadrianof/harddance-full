// --------------------------------------------------------------------
// React and Redux Imports
// --------------------------------------------------------------------

import React, { Component } from 'react';
import InlineSVG from 'svg-inline-react';

import bulletsIcon from 'bullets-horiz.svg?inline';
import labelIcon from 'icon-label.svg?inline';
import facebookIcon from 'icon-facebook.svg?inline';
import twitterIcon from 'icon-twitter.svg?inline';
import cartIcon from 'icon-cart.svg?inline';
import whatsIcon from 'icon-whatsapp.svg?inline';
import downloadIcon from 'icon-download.svg?inline';

// --------------------------------------------------------------------
// Define Component
// --------------------------------------------------------------------

export default class CardsFooterComponent extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.setState({
      opened: ''
    });
  }

  toggleOpenNav() {
    let foo = this.state.opened;

    switch(foo) {
      case '':
      case 'is--closed':
        foo = 'is--opened';
      break;
      case 'is--opened':
        foo = 'is--closed';
      break;
    }

    this.setState({
      opened: foo
    })
  }

  render() {
    return (
      <div className={ `hd-cards__footer ${ this.state.opened }` }>
        <nav className="hd-cards__nav">
          <ul className="hd-cards__share">
            <li className="hd-cards__social-icons hd-cards__social-icons--twitter">
              <a href={`https://twitter.com/intent/tweet?url=${ this.props.shareUrl }&hashtags=hardstyle,BrazilianHardTeam&text=${ this.props.shareText }`} target="_blank">
                <InlineSVG src={ twitterIcon } className="hd-u-color-blue-light" />
              </a>
            </li>
            <li className="hd-cards__social-icons hd-cards__social-icons--facebook">
              <a href={`https://www.facebook.com/sharer/sharer.php?u=${ this.props.shareUrl }`} target="_blank">
                <InlineSVG src={ facebookIcon } className="hd-u-color-blue" />
              </a>
            </li>
            <li className="hd-cards__social-icons hd-cards__social-icons--whatsapp">
              <a href={`whatsapp://send?text=${ this.props.shareText } ${ this.props.shareUrl }`} data-href={`${ this.props.shareUrl }`}>
                <InlineSVG src={ whatsIcon } className="hd-u-color-green" />
              </a>
            </li>
            {
              this.props.shopping && (
                <li className="hd-cards__social-icons hd-cards__social-icons--shopping">
                  <a href={ this.props.shopping } target="_blank">
                    <InlineSVG src={ cartIcon } className="hd-u-color-gray-strong" />
                  </a>
                </li>
              )
            }
            {
              this.props.gabbaLink && (
                <li className="hd-cards__social-icons hd-cards__social-icons--download">
                  <a href={ this.props.gabbaLink } target="_blank">
                    <InlineSVG src={ downloadIcon } className="hd-u-color-red" />
                  </a>
                </li>
              )
            }
          </ul>
        </nav>
        <div className="hd-cards__info">
          <span className="hd-cards__label hd-u-t-uppercase hd-u-tradegothic-small hd-u-color-gray-medium">
            <InlineSVG src={ this.props.icon || labelIcon } className="hd-u-mr-10 hd-u-color-gray-medium" /> <span className="hd-cards__copy">{ this.props.info }</span>
          </span>
          <InlineSVG src={ bulletsIcon } className="hd-cards__bullets" onClick={ this.toggleOpenNav.bind( this ) } />
        </div>
      </div>
    );
  }
}
