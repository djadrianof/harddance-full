// --------------------------------------------------------------------
// React and Redux Imports
// --------------------------------------------------------------------

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import InlineSVG from 'svg-inline-react';

import iconSkull from 'icon-skull.svg?inline';

// --------------------------------------------------------------------
// Define Component
// --------------------------------------------------------------------

class LoadingComponent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={`hd-loading ${ this.props.loaded ? 'is-finished' : '' }`}>
        <InlineSVG src={ iconSkull } className="hd-loading--icon hd-mb--10" />
        <div className="hd-loading--text hd-u-tradegothic-default hd-u-t-uppercase hd-u-color-gray-strong">
          <span>carregando</span>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  loaded: state.home.loaded
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(LoadingComponent);