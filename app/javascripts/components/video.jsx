// --------------------------------------------------------------------
// React and Redux Imports
// --------------------------------------------------------------------

import React, { Component } from 'react';

// --------------------------------------------------------------------
// Define Component
// --------------------------------------------------------------------

export default class VideoComponent extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.initialize();
  }

  initialize() {
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    window.onYouTubeIframeAPIReady = () => {
      this.player = new YT.Player(this.props.id, {
        startSeconds: 0,
        playerVars: {
          'autoplay'      : 0,
          'controls'      : 1,
          'rel'           : 0,
          'showinfo'      : 0,
          'modestbranding': 0,
          'iv_load_policy': 3
        },
        events: {
          'onReady': ( event ) => { this.props.onVideoReady( event ) },
          'onStateChange': ( event ) => { this.props.onVideoStateChange( event ) }
        }
      });
    }
  }

  render() {
    return (
      <div id={this.props.id} className={this.props.className}></div>
    );
  }
}
