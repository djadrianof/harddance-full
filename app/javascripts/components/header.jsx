// --------------------------------------------------------------------
// React and Redux Imports
// --------------------------------------------------------------------

import React, { Component } from 'react';

import hdLogo from 'hd-logo.png';

// --------------------------------------------------------------------
// Define Component
// --------------------------------------------------------------------

export default class HeaderComponent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="hd-header">
        <img src={hdLogo} className="hd-header-logo" />
      </div>
    );
  }
}
