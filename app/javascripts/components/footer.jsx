// --------------------------------------------------------------------
// React and Redux Imports
// --------------------------------------------------------------------

import React, { Component } from 'react';
import InlineSVG from 'svg-inline-react';

import hdLogo from 'hd-logo.png';
import facebookIcon from 'icon-facebook.svg?inline';
import twitterIcon from 'icon-twitter.svg?inline';
import whatsIcon from 'icon-whatsapp.svg?inline';

// --------------------------------------------------------------------
// Define Component
// --------------------------------------------------------------------

export default class FooterComponent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="hd-footer">
        <figure className="hd-footer__logo">
          <img src={hdLogo} />
        </figure>
        <dl className="hd-footer__social">
          <dt className="hd-u-tradegothic-default hd-u-t-uppercase hd-u-color-gray-strong">Compartilhe</dt>
          <dd>
            <a href="https://www.facebook.com/sharer/sharer.php?u=http://harddance.com.br" target="_blank">
              <InlineSVG src={ facebookIcon } className="hd-u-color-blue" />
            </a>
          </dd>
          <dd>
            <a href="https://twitter.com/intent/tweet?url=http://harddance.com.br&hashtags=qdance,hardstyle,BrazilianHardTeam&text=Fique%20por%20dentro%20das%20novidades%20do%20Hardstyle,%20ou%C3%A7a%20podcasts%20e%20muito%20mais!" target="_blank">
              <InlineSVG src={ twitterIcon } className="hd-u-color-blue-light" />
            </a>
          </dd>
          <dd>
            <a href="whatsapp://send?text=Fique%20por%20dentro%20das%20novidades%20do%20Hardstyle,%20ou%C3%A7a%20podcasts%20e%20muito%20mais! http://harddance.com.br" data-href="http://harddance.com.br">
              <InlineSVG src={ whatsIcon } className="hd-u-color-green" />
            </a>
          </dd>
        </dl>
      </div>
    );
  }
}
