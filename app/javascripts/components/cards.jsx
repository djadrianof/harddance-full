// --------------------------------------------------------------------
// React and Redux Imports
// --------------------------------------------------------------------

import React, { Component } from 'react';
import CardsFooter from 'components/cards-footer';

// --------------------------------------------------------------------
// Define Component
// --------------------------------------------------------------------

export default class CardsComponent extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="hd-cards">
        <div className="hd-cards__header">
          <h3 className="hd-cards__title hd-u-t-uppercase hd-u-futura-small hd-u-color-gray-strong">{ this.props.title }</h3>
          <p className="hd-cards__subtitle hd-u-t-uppercase hd-u-tradegothic-default hd-u-mt-5 hd-u-color-gray-medium hd-u-mb-0">{ this.props.subtitle }</p>
        </div>
        <figure className="hd-cards__image" onClick={ this.props.action || null }>
          <img src={ this.props.image } />
        </figure>
        <CardsFooter
          info={ this.props.info }
          icon={ this.props.icon }
          shopping={ this.props.shopping }
          gabbaLink={ this.props.gabbaLink }
          shareUrl={ this.props.shareUrl }
          shareText={ this.props.shareText } />
      </div>
    );
  }
}
