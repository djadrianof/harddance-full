// ------------------------------------------------------
// NPM Imports
// ------------------------------------------------------

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';

// ------------------------------------------------------
// App Imports
// ------------------------------------------------------

import configureStore from './stores/configure';
import App from './containers/app';
import Home from './pages/home';
import Events from './pages/events';

// ------------------------------------------------------
// App Stylesheets
// ------------------------------------------------------

import AppStyle from 'app';

// ------------------------------------------------------
// Constants
// ------------------------------------------------------

const app   = document.getElementById('hd');
const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

const auth = (nextState, replace) => {
  replace('events');
}

render(
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={App}>
        <IndexRoute component={Home} />
        <Route path="videos/:videoId" component={Home}/>
        <Route path="events" component={Events}>
          <Route path="foo" component={Events} />
        </Route>
      </Route>
    </Router>
  </Provider>,
  app
);
