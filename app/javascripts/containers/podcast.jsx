// --------------------------------------------------------------------
// React and Redux Imports
// --------------------------------------------------------------------

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import InlineSVG from 'svg-inline-react';

import Card from 'components/cards';

// --------------------------------------------------------------------
// Load Actions
// --------------------------------------------------------------------

import { podcastActions } from 'actions/index';


import bulletsIcon from 'bullets-horiz.svg?inline';
import playedIcon from 'icon-played.svg?inline';

// --------------------------------------------------------------------
// Define the Container Component
// --------------------------------------------------------------------

class Podcast extends Component {

  constructor(props) {
    super(props);
  }

  openPodcast( url ) {
    window.open( url );
  }

  getPodcastEdition( text ) {
    let isHandsUpInTheAir = text.includes( 'Hands Up In the Air' );
    let getEpisodeNumber = text.split( ' ' ).slice( -1 );
    let getPodcastName = text.split( '-' );
    return isHandsUpInTheAir ? getEpisodeNumber : getPodcastName[ 1 ];
  }

  render() {
    return (
      <section className="hd-podcast hd-section">
        <header className="hd-section__header">
          <h2 className="hd-section__title">Hands up in the air</h2>
          <p className="hd-section__text">Toda semana uma edição diferente com os lançamentos e hits do Euphoric e RAW Hardstyle mixados pelo DJ Adriano Fernandes</p>
        </header>
        <ul className="hd-grid-list hd-grid-list--horizontal">
          {
            this.props.podcast.map((item, i) => {
              return(
                <li key={i} className="hd-grid-list__item">
                  <Card
                    title={ `Episode ${ this.getPodcastEdition( item.name ) }` }
                    subtitle="Hands Up In the Air"
                    image={ item.pictures[ '640wx640h' ] }
                    info={ item.play_count }
                    icon={ playedIcon }
                    shareUrl={ item.url }
                    shareText={ `DJ Adriano Fernandes Presents - Hands Up In the Air (Episode 53)` }
                    action={ this.openPodcast.bind( this, item.url ) } />
                </li>
              )
            })
          }
        </ul>
      </section>
    )
  }
}

const mapStateToProps = (state) => ({
  podcast: state.podcast.podcasts
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(podcastActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Podcast);
