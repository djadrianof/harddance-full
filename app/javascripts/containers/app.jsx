// --------------------------------------------------------------------
// React and Redux Imports
// --------------------------------------------------------------------

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Hero from 'containers/hero';
import Footer from 'components/footer';

// --------------------------------------------------------------------
// Load Actions
// --------------------------------------------------------------------

import { homeActions } from '../actions/index';

// --------------------------------------------------------------------
// Define the Container Component
// --------------------------------------------------------------------

class App extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="hd-content">
        { this.props.children }
        <Footer />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  loading: state.home.loading
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(homeActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
