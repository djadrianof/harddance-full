// --------------------------------------------------------------------
// React and Redux Imports
// --------------------------------------------------------------------

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link, browserHistory } from 'react-router';
import InlineSVG from 'svg-inline-react';

import { homeActions } from '../actions/index';

import Video from 'components/video';
import videoIcon from 'icon-video.svg?inline';

// --------------------------------------------------------------------
// Define Component
// --------------------------------------------------------------------

class Hero extends Component {
  constructor(props) {
    super(props);
    this.heroRendered = false;
  }

  componentWillMount() {
    this.setState({
      videoTitle: null,
      videoPlaylist: ['hFSQbqAZT0g', '7vOJH7iYEWI', 'r9KraEl8qUI', 'ujO1DsJMi8Q', 'MX6A8oysN50', '0BW-XOY34w0']
    });
  }

  componentDidUpdate( prevProps, prevState ) {
    this.controlStateVideoPlayer( prevProps );
  }

  controlStateVideoPlayer( prevProps ) {
    if( this.props.loaded ) {
      if( this.props.playerOpened ) {
        if( this.videoPlayer ) this.videoPlayer.pauseVideo();
      } else {
        if( this.isMobile() ) {
          if( prevProps.videoTitle ) {
            if( this.videoPlayer ) this.videoPlayer.playVideo();
          }
        } else {
          if( this.videoPlayer ) this.videoPlayer.playVideo();
        }
      }
    }
  }

  isMobile() {
    return window.matchMedia(`screen and (max-width: 767px)`).matches;
  }

  playVideo( evt ) {
    this.videoPlayer = evt.target;

    if( !this.props.playerOpened ) {
      evt.target.mute();

      if( this.isMobile() ) {
        evt.target.cuePlaylist({ playlist: this.state.videoPlaylist });
      } else {
        evt.target.loadPlaylist({ playlist: this.state.videoPlaylist });
      }

      evt.target.setLoop( true );
      evt.target.setShuffle({ 'shufflePlaylist': true });
    }
  }

  setVideoTitle( title ) {
    let newState = Object.assign( {}, this.state );
    newState.videoTitle = title;

    if( title ) this.setState( newState );
  }

  videoPlayerState( evt ) {
    let videoTitle = evt.target.getVideoData().title;

    if( this.isMobile() ) {
      if( evt.data === YT.PlayerState.CUED || evt.data === YT.PlayerState.PLAYING ) {
        this.setVideoTitle( videoTitle );
      }
    } else {
      if( evt.data === YT.PlayerState.PLAYING ) {
        this.setVideoTitle( videoTitle );
      }
    }
  }

  render() {
    return (
      <section className="hd-hero">
        <div className="hd-hero__video">
          <Video id="hd-hero__player" onVideoReady={ this.playVideo.bind( this ) } onVideoStateChange={ this.videoPlayerState.bind( this ) } />
        </div>
        <aside className="hd-hero__info hd-u-color-gray-strong">
          <InlineSVG src={ videoIcon } className="hd-u-icon hd-u-mr-10" /> <span className="hd-u-tradegothic-small hd-u-t-uppercase">{ this.state.videoTitle }</span>
        </aside>
      </section>
    );
  }
}

const mapStateToProps = (state) => ({
  loading: state.home.loading,
  loaded: state.home.loaded,
  playerOpened: state.player.open
});

const mapDispatchToProps = (dispatch) => ({
  homeActions: bindActionCreators(homeActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Hero);