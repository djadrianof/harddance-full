// --------------------------------------------------------------------
// React and Redux Imports
// --------------------------------------------------------------------

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link, browserHistory } from 'react-router';
import InlineSVG from 'svg-inline-react';

// --------------------------------------------------------------------
// Load Actions
// --------------------------------------------------------------------

import { homeActions, playerActions } from 'actions/index';

import Card from 'components/cards';

import bulletsIcon from 'bullets-horiz.svg?inline';
import labelIcon from 'icon-label.svg?inline';

// --------------------------------------------------------------------
// Define the Container Component
// --------------------------------------------------------------------

class Music extends Component {

  constructor(props) {
    super(props);
  }

  loadMore() {
    this.props.homeActions.getMoreContent( this.props.currentPage + 1 );
  }

  playMusic( videoId ) {
    browserHistory.push(`/videos/${videoId}`);
  }

  loadMoreButtonContent() {
    if( this.props.loading ) {
      return(<span>Carregando...</span>);
    } else {
      return(<span>Ver mais</span>)
    }
  }

  playVideo( videoId ) {
    browserHistory.push(`/videos/${ videoId }`);
  }

  render() {
    return (
      <div className="hd-section">
        <header className="hd-section__header">
          <h2 className="hd-section__title">Hardstyle News</h2>
          <p className="hd-section__text">Confira os últimos lançamentos do Hardstyle em primeira mão</p>
        </header>
        <ul className="hd-grid-list">
          {
            this.props.music.map((item, i) => {
              return(
                <li key={i} className="hd-grid-list__item">
                  <Card
                    title={ item.artist }
                    subtitle={ item.music }
                    image={ item.image }
                    info={ item.label }
                    shopping={ item.shopping }
                    gabbaLink={ item.gabbaLink }
                    action={ this.playVideo.bind( this, item.id ) } />
                </li>
              )
            })
          }
        </ul>
        <nav className="hd-loadmore">
          <button className="hd-button hd-u-bg-gray-strong hd-u-color-white" onClick={ this.loadMore.bind(this) } disabled={`${this.props.loading ? 'disabled' : ''}`}>
            { this.loadMoreButtonContent() }
          </button>
        </nav>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  music: state.music.musics,
  currentPage: state.music.currentPage,
  loading: state.music.loading
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(playerActions, dispatch),
  homeActions: bindActionCreators(homeActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Music);
