// --------------------------------------------------------------------
// React and Redux Imports
// --------------------------------------------------------------------

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import InlineSVG from 'svg-inline-react';

import facebookIcon from 'icon-facebook.svg?inline';
import twitterIcon from 'icon-twitter.svg?inline';
import whatsIcon from 'icon-whatsapp.svg?inline';
import closeIcon from 'icon-close.svg?inline';

// --------------------------------------------------------------------
// Load Actions
// --------------------------------------------------------------------

import { playerActions } from '../actions/index';

// --------------------------------------------------------------------
// Define the Container Component
// --------------------------------------------------------------------

class Player extends Component {

  constructor(props) {
    super(props);
  }

  closePlayer() {
    this.props.actions.close();
  }

  render() {
    return (
      <div className="hd-player" onClick={ this.closePlayer.bind(this) }>
        {/* <iframe src={`https://www.youtube.com/embed/${this.props.player.video.id}?showinfo=0&modestbranding=0&iv_load_policy=3`} frameBorder="0" allowFullScreen></iframe> */}
        <video controls name="media">
          <source src={`https://preview.content.hardstyle.com/index2.php?id=${this.props.player.video.id}`} type="audio/mpeg" />
        </video>
        <nav className="hd-player__share">
          <dl className="hd-player__social">
            <dt className="hd-u-tradegothic-default hd-u-t-uppercase hd-u-color-gray-medium">Compartilhe</dt>
            <dd>
              <a href={`https://www.facebook.com/sharer/sharer.php?u=http://harddance.com.br/share/videos/${this.props.player.video.id}`} target="_blank">
                <InlineSVG src={ facebookIcon } className="hd-u-color-blue" />
              </a>
            </dd>
            <dd>
              <a href={`https://twitter.com/intent/tweet?url=http://harddance.com.br/share/videos/${this.props.player.video.id}&hashtags=hardstyle,BrazilianHardTeam&text=${this.props.player.video.title}`} target="_blank">
                <InlineSVG src={ twitterIcon } className="hd-u-color-blue-light" />
              </a>
            </dd>
            <dd>
              <a href={`whatsapp://send?text=${this.props.player.video.title} http://harddance.com.br/share/videos/${this.props.player.video.id}`} data-href={`http://harddance.com.br/share/videos/${this.props.player.video.id}`}>
                <InlineSVG src={ whatsIcon } className="hd-u-color-green" />
              </a>
            </dd>
          </dl>
        </nav>
        <InlineSVG src={ closeIcon } className="hd-player__close hd-u-color-white" />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  player: state.player
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(playerActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Player);
