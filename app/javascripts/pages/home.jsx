// --------------------------------------------------------------------
// React and Redux Imports
// --------------------------------------------------------------------

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Loading from 'components/loading';
import Music from 'containers/music';
import Podcast from 'containers/podcast';
import Player from 'containers/player';

// --------------------------------------------------------------------
// Load Actions
// --------------------------------------------------------------------

import { homeActions, playerActions, musicActions } from 'actions/index';

// --------------------------------------------------------------------
// Define the Container Component
// --------------------------------------------------------------------

class Home extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.getHomeData();
  }

  componentDidUpdate() {
    if( this.props.loaded && !this.props.playerOpened ) {
      this.hasVideoAndOpenPlayer();
    }
  }

  hasVideoAndOpenPlayer() {
    let videoId = this.props.routeParams.videoId;

    if( videoId ) {
        let videoProps = {
          id: videoId,
          title: ''
        };

      this.props.playerActions.open( videoProps );
    }
  }

  getHomeData() {
    this.props.actions.getContent();
  }

  render() {
    return (
      <div className="hd-content">
        <Music />
        <Loading />
        {this.props.playerOpened &&
          <Player />
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  loading: state.home.loading,
  loaded: state.home.loaded,
  playerOpened: state.player.open,
  musics: state.music.musics
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(homeActions, dispatch),
  playerActions: bindActionCreators(playerActions, dispatch),
  musicActions: bindActionCreators(musicActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
