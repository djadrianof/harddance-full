// --------------------------------------------------------------------
// React and Redux Imports
// --------------------------------------------------------------------

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router';

// --------------------------------------------------------------------
// Load Actions
// --------------------------------------------------------------------

import { homeActions, playerActions } from '../actions/index';

// --------------------------------------------------------------------
// Define the Container Component
// --------------------------------------------------------------------

class Events extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    console.log(this.props)
  }

  componentDidUpdate() {

  }

  foo() {
    this.props.router.push('/events/foo');
  }

  render() {
    return (
      <div className="hd-events">
        <h2>Events Page</h2>
        <span onClick={this.foo.bind(this)}>Click to change route</span>
        {this.props.children}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  loading: state.home.loading,
  loaded: state.home.loaded,
  playerOpened: state.player.open,
  musics: state.music.musics
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(homeActions, dispatch),
  playerActions: bindActionCreators(playerActions, dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Events));
