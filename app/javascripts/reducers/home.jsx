// -----------------------------------------------------
// Define the state of Component
// -----------------------------------------------------

const homeState = {
  loading: false,
  loaded: false
};

// -----------------------------------------------------
// Subscribe the actions of component
// -----------------------------------------------------

const appReducer = (state = homeState, action) => {
  let newState = Object.assign({}, state);

  switch( action.type ) {
    case 'HOME_LOADING':
      newState.loading = true;
      return newState;

    case 'HOME_LOADED':
      newState.loading = false;
      newState.loaded = true;
      return newState;

    default:
      return state;
  }
};

export default appReducer;
