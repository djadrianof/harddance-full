// -----------------------------------------------------
// Define the state of Component
// -----------------------------------------------------

const playerState = {
  video: null,
  open: false
};

// -----------------------------------------------------
// Subscribe the actions of component
// -----------------------------------------------------

const playerReducer = (state = playerState, action) => {
  let newState = Object.assign({}, state);

  switch( action.type ) {
    case 'PLAYER_OPEN':
      newState.video = action.video;
      newState.open = true;
      return newState;

    case 'PLAYER_CLOSE':
      newState.video = null;
      newState.open = false;
      return newState;

    default:
      return state;
  }
};

export default playerReducer;
