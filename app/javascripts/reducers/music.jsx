// -----------------------------------------------------
// Define the state of Component
// -----------------------------------------------------

const musicState = {
  musics: [],
  loading: false,
  currentPage: 1
};

// -----------------------------------------------------
// Subscribe the actions of component
// -----------------------------------------------------

const musicReducer = (state = musicState, action) => {
  let newState = Object.assign({}, state);

  switch( action.type ) {
    case 'MUSIC_LOADING':
      newState.loading = true;
      return newState;

    case 'MUSIC_LOADED':
      newState.musics = action.musics;
      newState.loading = false;
      return newState;

    case 'MUSIC_MORE_LOADED':
      newState.musics = action.musics;
      newState.currentPage = action.page;
      newState.loading = false;
      return newState;

    default:
      return state;
  }
};

export default musicReducer;
