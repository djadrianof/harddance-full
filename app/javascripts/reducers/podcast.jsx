// -----------------------------------------------------
// Define the state of Component
// -----------------------------------------------------

const podcastState = {
  podcasts: [],
  loading: false
};

// -----------------------------------------------------
// Subscribe the actions of component
// -----------------------------------------------------

const musicReducer = (state = podcastState, action) => {
  let newState = Object.assign({}, state);

  switch( action.type ) {
    case 'PODCAST_LOADING':
      newState.loading = true;
      return newState;

    case 'PODCAST_LOADED':
      newState.podcasts = action.podcasts;
      newState.loading = false;
      return newState;

    default:
      return state;
  }
};

export default musicReducer;
