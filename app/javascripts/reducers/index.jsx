// -----------------------------------------------------
// Redux Imports
// -----------------------------------------------------

import { combineReducers } from 'redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';

// -----------------------------------------------------
// Load Reducers
// -----------------------------------------------------

import homeReducer    from './home';
import musicReducer   from './music';
import podcastReducer from './podcast';
import playerReducer  from './player';

// -----------------------------------------------------
// Share reducers to application
// -----------------------------------------------------

const reducers = combineReducers({
  home   : homeReducer,
  music  : musicReducer,
  podcast: podcastReducer,
  player : playerReducer,
  routing: routerReducer
});

export default reducers;
