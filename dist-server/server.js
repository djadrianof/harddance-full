'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _socket = require('socket.io');

var _socket2 = _interopRequireDefault(_socket);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _compression = require('compression');

var _compression2 = _interopRequireDefault(_compression);

var _routes = require('./routes');

var _routes2 = _interopRequireDefault(_routes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ---------------------------------------------------
// Constants
// ---------------------------------------------------

// ---------------------------------------------------
// Import NPM Libs
// ---------------------------------------------------

var app = (0, _express2.default)();

// ---------------------------------------------------
// Import Helpers
// ---------------------------------------------------

var httpServer = _http2.default.Server(app);
var router = _express2.default.Router();

var isProduction = process.env.NODE_ENV === 'production' || false;
var port = 3000;

// if (!port) port = 3000;

app.enable('trust proxy');

app.use((0, _compression2.default)());
app.use('/', _routes2.default);

// ---------------------------------------------------
// Define template engine
// ---------------------------------------------------

app.use(_express2.default.static(__dirname + '/../dist'));
app.set('views', __dirname + '/../dist');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

// ---------------------------------------------------
// Start Server
// ---------------------------------------------------

var server = httpServer.listen(process.env.PORT || port, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});

// ---------------------------------------------------
// Start Webpack Server (Only Development)
// ---------------------------------------------------

if (!isProduction) {
  var webpack = require('webpack');
  var WebpackDevServer = require('webpack-dev-server');
  var webpackConfig = require('../webpack.config.js');

  new WebpackDevServer(webpack(webpackConfig), {
    hot: false,
    noInfo: true,
    quiet: false,
    historyApiFallback: true,
    proxy: {
      '/api/*': {
        target: 'http://0.0.0.0:3000',
        secure: false
      }
    },
    stats: { colors: true }
  }).listen(8080, '0.0.0.0', function (err) {
    if (err) console.log(err);
    console.log('Webpack Dev Server listening at 8080');
  });
}