'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _q = require('q');

var _q2 = _interopRequireDefault(_q);

var _posts = require('../classes/posts');

var _posts2 = _interopRequireDefault(_posts);

var _podcasts = require('../classes/podcasts');

var _podcasts2 = _interopRequireDefault(_podcasts);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.get('/home', (0, _cors2.default)(), function (req, res) {

  var posts = new _posts2.default();
  var podcasts = new _podcasts2.default();

  _q2.default.all([posts.getPosts(), podcasts.getPodcasts()]).then(function (data) {
    res.json({
      music: data[0],
      podcasts: data[1]
    });
  });
});

router.get('/home/:page', (0, _cors2.default)(), function (req, res) {

  var posts = new _posts2.default();

  _q2.default.all([posts.getPosts(req.params.page)]).then(function (data) {
    res.json({
      music: data[0]
    });
  });
});

exports.default = router;