'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _requestPromise = require('request-promise');

var _requestPromise2 = _interopRequireDefault(_requestPromise);

var _q = require('q');

var _q2 = _interopRequireDefault(_q);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Podcasts = function () {
  function Podcasts() {
    _classCallCheck(this, Podcasts);
  }

  _createClass(Podcasts, [{
    key: 'getPodcasts',
    value: function getPodcasts() {

      var options = {
        uri: 'https://api.mixcloud.com/djadriano/cloudcasts',
        headers: { 'User-Agent': 'Request-Promise' },
        json: true
      };

      return (0, _requestPromise2.default)(options);
    }
  }]);

  return Podcasts;
}();

exports.default = Podcasts;