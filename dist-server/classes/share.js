'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _requestPromise = require('request-promise');

var _requestPromise2 = _interopRequireDefault(_requestPromise);

var _cheerio = require('cheerio');

var _cheerio2 = _interopRequireDefault(_cheerio);

var _googleapis = require('googleapis');

var _q = require('q');

var _q2 = _interopRequireDefault(_q);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var youtube = _googleapis.google.youtube({
  version: 'v3',
  auth: 'AIzaSyAdbILc-FIh3ufqQ8ywX6BRYjkTD8HMZpA'
});

var Share = function () {
  function Share() {
    _classCallCheck(this, Share);
  }

  _createClass(Share, [{
    key: 'getVideoInfo',
    value: function getVideoInfo(videoId) {

      var deferred = _q2.default.defer();

      var queryOptions = {
        'part': 'snippet',
        'maxResults': 1,
        'videoCategoryId': 10,
        'id': videoId
      };

      youtube.videos.list(queryOptions, function (err, data) {
        err ? deferred.reject(result) : deferred.resolve({ data: data });
      });

      return _q2.default.all(deferred.promise);
    }
  }]);

  return Share;
}();

exports.default = Share;