'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _requestPromise = require('request-promise');

var _requestPromise2 = _interopRequireDefault(_requestPromise);

var _cheerio = require('cheerio');

var _cheerio2 = _interopRequireDefault(_cheerio);

var _googleapis = require('googleapis');

var _q = require('q');

var _q2 = _interopRequireDefault(_q);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var youtube = _googleapis.google.youtube({
  version: 'v3',
  auth: 'AIzaSyAdbILc-FIh3ufqQ8ywX6BRYjkTD8HMZpA'
});

var Posts = function () {
  function Posts() {
    _classCallCheck(this, Posts);

    this.arrPosts = [];
    this.arrPostsVideos = [];
    this.currentPage = 0;
  }

  _createClass(Posts, [{
    key: 'getPosts',
    value: function getPosts() {
      var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;


      var paramPage = '';

      if (page) {
        paramPage = '/page/' + page;
      }

      var url = 'https://www.hardstyle.com/hardstyle-releases/tracks' + paramPage;

      var options = {
        uri: url,
        transform: function transform(body) {
          return _cheerio2.default.load(body);
        }
      };

      return (0, _requestPromise2.default)(options).then(this.storePosts.bind(this));
    }
  }, {
    key: 'storePosts',
    value: function storePosts($) {
      var _this = this;

      var posts = $('table.list tr');
      var promises = [];

      var arrFoo = posts.filter(function (i, item) {
        return item.attribs.class != undefined;
      });

      var _loop = function _loop(i) {
        var deferred = _q2.default.defer();

        var postsDataImage = $(arrFoo[i]).find('.image img')[0];
        var postsData = $(arrFoo[i]).find('.text-1 a')[0];
        var postsInfoData = $(arrFoo[i]).find('.text-2 a')[0];
        var postsDataLink = postsData.attribs.href;
        var postsDataId = null;

        if (postsDataLink) {
          postsDataId = postsDataLink.split('/').pop();
        }

        var artistName = postsData.children[2] != undefined ? postsData.children[2] : postsData.children[1];
        var musicName = postsData.children[0] || null;
        var labelName = postsInfoData.children[2] || null;
        var musicImage = postsDataImage.attribs.src ? postsDataImage.attribs.src.replace('48x48', '500x500') : null;
        var artistAndMusic = artistName.children && musicName.children ? artistName.children[0].data + ' - ' + musicName.children[0].data : null;
        var titleSearchGabba = artistAndMusic ? artistAndMusic.split(' ').join('+') : null;
        titleSearchGabba = titleSearchGabba.replace('\'', '');

        var postData = {
          artist: artistName.children ? artistName.children[0].data : null,
          music: musicName.children ? musicName.children[0].data : null,
          label: labelName.children ? labelName.children[0].data : null,
          image: musicImage,
          id: postsDataId,
          shopping: postsData.attribs.href,
          gabbaLink: null
        };

        var promise = _this.getGabbaLink(titleSearchGabba).then(function ($) {

          var itemSearchList = $('div.views-row-first .ttl4reg');

          if (itemSearchList) {
            if (itemSearchList.length && itemSearchList[0].hasOwnProperty('attribs')) {
              postData.gabbaLink = 'http://1gabba.net' + itemSearchList[0].attribs.href;
            }

            deferred.resolve(postData);
          } else {
            deferred.reject();
          }
        });

        promises.push(deferred.promise);
      };

      for (var i = 0; i < arrFoo.length; i++) {
        _loop(i);
      }

      return _q2.default.all(promises).then(function (data) {
        // remove musics duplicated on array
        _this.arrPosts = data.reduce(function (field, e1) {
          var matches = field.filter(function (e2) {
            return e1.music == e2.music;
          });
          if (matches.length == 0) {
            field.push(e1);
          }
          return field;
        }, []);

        return _this.arrPosts.filter(function (item) {
          return item.gabbaLink;
        });
      });
    }
  }, {
    key: 'getGabbaLink',
    value: function getGabbaLink(title) {
      var options = {
        uri: 'http://1gabba.net/frontpage?title=' + title,
        transform: function transform(body) {
          return _cheerio2.default.load(body);
        }
      };

      return (0, _requestPromise2.default)(options);
    }
  }, {
    key: 'getYoutubeVideos',
    value: function getYoutubeVideos() {

      var promises = [];

      this.arrPosts.forEach(function (item, index) {

        var deferred = _q2.default.defer();

        var queryOptions = {
          'part': 'snippet',
          'maxResults': 1,
          'type': 'video',
          'videoDuration': 'any',
          'videoCategoryId': 10,
          'q': '"' + item.artist + ' - ' + item.music + '"'
        };

        var promise = youtube.search.list(queryOptions, function (err, data) {
          if (err) {
            deferred.reject(data);
          } else {
            if (data.items.length) {
              if (data.items[0].snippet.title.includes(item.artist + ' - ' + item.music)) {
                deferred.resolve({ info: item, data: data });
              } else {
                deferred.resolve({ info: item, data: { items: [] } });
              }
            } else {
              deferred.resolve({ info: item, data: data });
            }
          }
        });

        promises.push(deferred.promise);
      });

      return _q2.default.all(promises).then(function (data) {
        var dataFiltered = data.filter(function (item) {
          if (item.data.items.length) return item;
        });

        return dataFiltered;
      });
    }
  }]);

  return Posts;
}();

exports.default = Posts;