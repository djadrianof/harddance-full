// ------------------------------------------------------------------
// Default Dependencies
// ------------------------------------------------------------------

var path              = require('path');
var webpack           = require('webpack');
var precss            = require('precss');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CompressionPlugin = require( "compression-webpack-plugin" );
var rootPath          = path.resolve( __dirname );

// ------------------------------------------------------------------
// Postcss Plugins
// ------------------------------------------------------------------

var autoprefixer  = require('autoprefixer');
var postcssImport = require('postcss-import');
var stylelint     = require('stylelint');
var cssMqPacker   = require('css-mqpacker');
var fontMagician  = require('postcss-font-magician');
var cssEasings    = require('postcss-easings');
var pxtorem       = require('postcss-pxtorem');
var cssnano       = require('cssnano');

// ------------------------------------------------------------------
// Initialize Config
// ------------------------------------------------------------------

var isProd = (process.env.NODE_ENV === 'production');

var webpackPlugins = {
  development: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      template: './layouts/index.html'
    }),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': process.env.NODE_ENV
      }
    }),
    new ExtractTextPlugin("[name].css")
  ],
  production: [
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new CompressionPlugin( {
      asset: "[path].gz[query]",
      algorithm: "gzip",
      test: /\.js$|\.html|\.css$/,
      threshold: 10240,
      minRatio: 0.8
    }),
    new webpack.optimize.DedupePlugin(),
    new HtmlWebpackPlugin({
      template: './layouts/index.html',
      favicon: './favicon.ico'
    }),
    new ExtractTextPlugin("[name].css")
  ]
};

var postCssPlugins = {
  development: [
    precss,
    autoprefixer,
    cssEasings,
    fontMagician({
      hosted: './app/assets/fonts'
    }),
    postcssImport({
      addDependencyTo: webpack
    })
  ],
  production: [
    cssnano(),
    precss,
    autoprefixer,
    cssEasings
  ]
};

function getWebpackPlugins() {
  return isProd ? webpackPlugins.production : webpackPlugins.development;
}

function getPostCssPlugins(webpack) {
  return isProd ? postCssPlugins.production : postCssPlugins.development;
}

function getEntry() {
  var arrEntry = ['./app/javascripts/index.jsx'];

  if( !isProd ) {
    arrEntry.push('webpack-dev-server/client?http://0.0.0.0:8080');
    arrEntry.push('webpack/hot/only-dev-server');
  }

  return arrEntry;
}

module.exports = {
  devtool: isProd ? 'source-map' : 'cheap-module-source-map',
  entry: getEntry(),
  output: {
    path: path.resolve(rootPath, 'dist'),
    filename: "bundle.js",
    publicPath: '/'
  },
  module: {
     loaders: [
       {
         test: /\.jsx?$/,
         exclude: /(node_modules)/,
         loaders: ['react-hot', 'babel']
       },
       {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader!postcss-loader!sass-loader')
       },
       {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader!postcss-loader')
       },
       {
        test: /\.(svg|png|jpe?g|gif|ico|svg)$/,
        loader: 'file-loader?name=app/assets/images/[name].[ext]'
       },
       {
        test: /\.(svg\?inline)$/i,
        loader: 'svg-inline'
       },
       {
        test: /\.(mp4|mov|webm)$/,
        loader: 'file-loader?name=app/assets/videos/[name].[ext]'
       },
       {
        test: /\.(ttf|eot|otf|woff(2)?)(\w+)?$/,
        loader: 'file-loader?name=app/assets/fonts/[name].[ext]'
       }
     ]
   },
   resolve: {
     root: [
       path.resolve('./app/assets'),
       path.resolve('./app/assets/images'),
       path.resolve('./app/javascripts'),
       path.resolve('./app/stylesheets')
     ],
     extensions: ['', '.js', '.jsx', '.scss', '.svg']
   },
   postcss: function (webpack) {
      return getPostCssPlugins(webpack);
   },
   plugins: getWebpackPlugins()
}
