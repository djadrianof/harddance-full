import express from 'express';
import path    from 'path';
import cors    from 'cors';
import Q       from 'q';

import PostsClass    from '../classes/posts';
import PodcastsClass from '../classes/podcasts';
import ShareClass from '../classes/share';

const router = express.Router();

// --------------------------------------------------------------------
// Index Routes
// --------------------------------------------------------------------

router.get('/', (req, res) => {
  res.render('index.html');
});

// --------------------------------------------------------------------
// Videos Routes
// --------------------------------------------------------------------

router.get('/videos/*', (req, res) => {
  res.render('index.html');
});

router.get('/share/videos/:id', (req, res) => {
  let share = new ShareClass;

  share.getVideoInfo(req.params.id).then((data) => {
    let videoData = data.data.items[ 0 ].snippet;
    let videoId   = data.data.items[ 0 ].id;

    res.render('views/share.html', {
      videoTitle: videoData.title,
      videoDescription: videoData.description,
      videoImage: videoData.thumbnails.high.url,
      videoId: videoId
    });
  });
});

// --------------------------------------------------------------------
// Api Routes
// --------------------------------------------------------------------

router.get('/api/home', cors(), (req, res) => {

  let posts    = new PostsClass;
  let podcasts = new PodcastsClass;

  Q.all([
    posts.getPosts(),
    podcasts.getPodcasts()
  ]).then((data) => {
    res.json({
      music: data[0],
      podcasts: data[1]
    });
  })

});

router.get('/api/home/:page', cors(), function (req, res) {

  let posts = new PostsClass;

  Q.all([
    posts.getPosts( req.params.page )
  ]).then((data) => {
    res.json({
      music: data[0]
    });
  })

});

router.get('/api/videos/:id', (req, res) => {
  let share = new ShareClass;

  share.getVideoInfo(req.params.id).then((data) => {
    let videoData = data.data.items[ 0 ].snippet;
    let videoId   = data.data.items[ 0 ].id;

    res.json({
      videoTitle: videoData.title,
      videoDescription: videoData.description,
      videoImage: videoData.thumbnails.high.url,
      videoId: videoId
    });
  });
});

router.get('/api/mock/home', cors(), (req, res) => {

  res.json({
    music: [
    {
      "info": {
        "title": "Mandy - Here We Go",
        "artist": "Mandy",
        "music": "Here We Go",
        "label": "Dirty Workz",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14709547.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/GI_azvlOp4bUeASPYNFlWHXRvUs\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 119,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/Gsqg6m9RKRsUZUqHGKp-2Q19m8A\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "7D6zxvkTfkE"
            },
            "snippet": {
              "publishedAt": "2016-10-26T17:00:00.000Z",
              "channelId": "UCt8hUmML7zjM6effdZ3Ip5A",
              "title": "Mandy - Here We Go (Official HQ Preview)",
              "description": "DOWNLOAD: https://DirtyWorkz.lnk.to/HereWeGoYL ◉ Subscribe to Dirty Workz: http://bit.ly/DWX_Subscribe ▽Subscribe to our Spotify playlist: ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/7D6zxvkTfkE/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/7D6zxvkTfkE/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/7D6zxvkTfkE/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Dirty Workz",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "KANiKEN - 72 Hours",
        "artist": "KANiKEN",
        "music": "72 Hours",
        "label": "Crab Music Records",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14619128.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/0aVSkqEnIBiZOMz9rJfiRuWmQ1Y\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 8,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/t07SOD_c2x4klZEmSBBkYsTvpp8\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "AtAm_xtEgm4"
            },
            "snippet": {
              "publishedAt": "2016-11-06T01:28:38.000Z",
              "channelId": "UCHf3hFEBujr-O5yMNbCljkg",
              "title": "KANiKEN - 72 Hours(Original Mix)",
              "description": "KANiKEN 2nd album \"72 hours\" Nov.9th release!! Follow KANiKEN Youtube http://www.youtube.com/channel/UCHf3hFEBujr-O5yMNbCljkg/videos Twitter ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/AtAm_xtEgm4/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/AtAm_xtEgm4/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/AtAm_xtEgm4/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Kani Ken",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Modul8 - Infinity",
        "artist": "Modul8",
        "music": "Infinity",
        "label": "X-Bone",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14764845.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/ikbKBWZPO1iw46_Fla3QrUw8iyk\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 152,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/WM-BLwFSaEw-lghzSpyftdqPv9Q\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "zC-7aoxywMw"
            },
            "snippet": {
              "publishedAt": "2016-10-31T10:01:14.000Z",
              "channelId": "UC9PKCJ2kJEU-kR85ZyaN46w",
              "title": "Modul8 - Infinity (#XBONE138)",
              "description": "Out: 8-11-2016 Connect: www.facebook.com/djmodul8 twitter.com/djmodul8 www.instagram.com/djmodul8 Connect: http://www.xbone.nl/ ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/zC-7aoxywMw/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/zC-7aoxywMw/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/zC-7aoxywMw/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "X-BONE",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Eufeion - Badman Rock",
        "artist": "Eufeion",
        "music": "Badman Rock",
        "label": "Core Fever",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14616850.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/K3CkTiHZvIiAnaPSrnLBTrmVinE\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 17,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/ErhpuCnasKNlOkfs3YIA9HB1L-U\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "H4kl1JzCgRk"
            },
            "snippet": {
              "publishedAt": "2016-02-12T18:54:01.000Z",
              "channelId": "UCRgWBewp1mmTszULYJtH0Vg",
              "title": "Eufeion - Badman Rock",
              "description": "Track Info: Eufeion - Badman Rock - (Core Fever Records) Copywrite Core Fever Records 2016 http://www.eufeion.co.uk ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/H4kl1JzCgRk/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/H4kl1JzCgRk/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/H4kl1JzCgRk/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Eufeion",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Double F-ect - No Worries",
        "artist": "Double F-ect",
        "music": "No Worries",
        "label": "Activa Records",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14739414.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/w0qR4z2Q2z7ChLpSWGr1To6A8dY\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 1020,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/I7o5ZVf21Neh31I7bOvIhJ9Jxko\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "UUmcFVp44jk"
            },
            "snippet": {
              "publishedAt": "2016-11-06T10:04:48.000Z",
              "channelId": "UCLXyd1Dcc_V3ugBhYkzm8oQ",
              "title": "Double F-ect - No Worries (Preview)",
              "description": "Double F-ect - No Worries (Preview) ➲ Follow TheHardstyleMusicz: • Google+: [http://google.com/+thehardstylemusicz] • Facebook: ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/UUmcFVp44jk/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/UUmcFVp44jk/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/UUmcFVp44jk/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "TheHardstyleMusicz",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Wave Pressure - Back To You",
        "artist": "Wave Pressure",
        "music": "Back To You",
        "label": "Dirty Workz",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14709557.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/53xPbnlTHK9tlCKAZ-7Sh_fXTpk\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 243,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/pU1hG98hf-VkYPF4KSfVdH1DDKI\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "XAZ2ophp7Vo"
            },
            "snippet": {
              "publishedAt": "2016-10-25T17:00:02.000Z",
              "channelId": "UCt8hUmML7zjM6effdZ3Ip5A",
              "title": "Wave Pressure - Back To You (Official HQ Preview)",
              "description": "DOWNLOAD: https://DirtyWorkz.lnk.to/BackToYouYL ◉ Subscribe to Dirty Workz: http://bit.ly/DWX_Subscribe ▽Subscribe to our Spotify playlist: ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/XAZ2ophp7Vo/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/XAZ2ophp7Vo/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/XAZ2ophp7Vo/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Dirty Workz",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Hit 'n Run - Fear Is Not Real",
        "artist": "Hit 'n Run",
        "music": "Fear Is Not Real",
        "label": "Blackout Records",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14689333.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/LtA_IefUwk9JKFfltoXZ_FbSwUw\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 49,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/D8WCHrhVxyyTVdhYHhjcMQjOsG0\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "CuDcYH3I65c"
            },
            "snippet": {
              "publishedAt": "2016-10-27T16:29:06.000Z",
              "channelId": "UCE4yIxqocOw5_XIsCWp-2gA",
              "title": "Hit 'N Run - Fear Is Not Real (Available November 7)",
              "description": "Hit 'N Run - Fear Is Not Real (BKT035) Release date: November 7, 2016 Label: Blackout Rec Connect with Hit 'N Run: Website: http://www.hitnrundj.com ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/CuDcYH3I65c/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/CuDcYH3I65c/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/CuDcYH3I65c/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "DJHITNRUNMEDIA",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Physika - The Huntsman",
        "artist": "Physika",
        "music": "The Huntsman",
        "label": "Gearbox Revolutions",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14616825.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/ojT214iKQZZzSGcg6QuXTE2B4f4\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 2,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/J5XcWfcaZGsw3R6_zOz_GNWSh6Q\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "PIOh_jPFMG4"
            },
            "snippet": {
              "publishedAt": "2016-11-07T18:34:15.000Z",
              "channelId": "UCH-GFr_3jzTgx4Lvm50XD8w",
              "title": "Physika - The Huntsman [GBR006]",
              "description": "Follow us https://www.facebook.com/GearboxDigital https://instagram.com/gearboxdigital https://soundcloud.com/gearbox-digital.",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/PIOh_jPFMG4/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/PIOh_jPFMG4/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/PIOh_jPFMG4/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Gearbox Digital",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "ANDY SVGE - Gravity",
        "artist": "ANDY SVGE",
        "music": "Gravity",
        "label": "Q-dance",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14763330.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/iiQHy-zldfwZQEoEZGepJaDkPpU\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 887,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/8MPwtK3NIkmOToeZIi7hLhYtpTc\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "d9AKbaeecKI"
            },
            "snippet": {
              "publishedAt": "2016-07-05T11:15:25.000Z",
              "channelId": "UC9QmxPabyFhmSSE3VpXTC4A",
              "title": "ANDY SVGE - Gravity | Dragonblood EP",
              "description": "Out now: https://qdance.lnk.to/dragonbloodepSY • Subscribe: http://bit.ly/speqtrum-yts • Q-dance on Spotify: http://bit.ly/qdance-spotify ♫♫ More Music ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/d9AKbaeecKI/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/d9AKbaeecKI/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/d9AKbaeecKI/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Speqtrum Music",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Wildstylez - Encore",
        "artist": "Wildstylez",
        "music": "Encore",
        "label": "Q-dance",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14763342.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/TsrFvv5tLDlPmezcdQSRNoU7zwY\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 1945,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/seaB020CxnUJp8--6Hc6yEondpk\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "lesUnDTrXsU"
            },
            "snippet": {
              "publishedAt": "2016-07-05T11:15:50.000Z",
              "channelId": "UC9QmxPabyFhmSSE3VpXTC4A",
              "title": "Wildstylez - Encore | Dragonblood EP",
              "description": "Out now: https://qdance.lnk.to/dragonbloodepSY • Subscribe: http://bit.ly/speqtrum-yts • Q-dance on Spotify: http://bit.ly/qdance-spotify ♫♫ More Music ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/lesUnDTrXsU/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/lesUnDTrXsU/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/lesUnDTrXsU/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Speqtrum Music",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Cyber - Perception",
        "artist": "Cyber",
        "music": "Perception",
        "label": "Q-dance",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14763350.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/-dhKVk7EXJ38vcQLt1W2lUIQj9k\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 224,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/uk719-3QVz_1_QjHIzQx_waV0gs\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "O2w4d_kwDJI"
            },
            "snippet": {
              "publishedAt": "2016-07-05T11:15:13.000Z",
              "channelId": "UC9QmxPabyFhmSSE3VpXTC4A",
              "title": "Cyber - Perception | Dragonblood EP",
              "description": "Out now: https://qdance.lnk.to/dragonbloodepSY • Subscribe: http://bit.ly/speqtrum-yts • Q-dance on Spotify: http://bit.ly/qdance-spotify ♫♫ More Music ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/O2w4d_kwDJI/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/O2w4d_kwDJI/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/O2w4d_kwDJI/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Speqtrum Music",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Audiotricz - Inception",
        "artist": "Audiotricz",
        "music": "Inception",
        "label": "Q-dance",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14763424.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/ckER3r-2BuARbb1Cl199kYrChns\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 229,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/JTiKzBOVrClvVoiarMVolQOYJa8\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "hqNsZ0pTRxU"
            },
            "snippet": {
              "publishedAt": "2016-07-05T11:15:37.000Z",
              "channelId": "UC9QmxPabyFhmSSE3VpXTC4A",
              "title": "Audiotricz - Inception | Dragonblood EP",
              "description": "Out now: https://qdance.lnk.to/dragonbloodepSY • Subscribe: http://bit.ly/speqtrum-yts • Q-dance on Spotify: http://bit.ly/qdance-spotify ♫♫ More Music ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/hqNsZ0pTRxU/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/hqNsZ0pTRxU/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/hqNsZ0pTRxU/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Speqtrum Music",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Brutality - This Is War",
        "artist": "Brutality",
        "music": "This Is War",
        "label": "Q-dance presents NEXT",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14711166.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/6LUY-DvIEV-jHEd36jA15NruJP4\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 14,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/14kqbDg5oSCB3jGDBFsN_PrEODs\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "AlLHkEUzhbA"
            },
            "snippet": {
              "publishedAt": "2016-01-17T22:35:37.000Z",
              "channelId": "UCUN6tCL_zTKDH1H0enP_bnA",
              "title": "Warface VS Sub Sonik Played \"Brutality - This Is War\" @ Future of Raw VS Legends (16.01.16)",
              "description": "",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/AlLHkEUzhbA/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/AlLHkEUzhbA/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/AlLHkEUzhbA/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "CriMiNaL Music",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Baz - I Will",
        "artist": "Baz",
        "music": "I Will",
        "label": "Relay Records",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14759980.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/HX27dQWVfjP5dEPugRey8MHx5m4\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 3,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/0gUath2v2h6ZKzBCMYGTBxYyQ0k\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "UtfJuW74o3U"
            },
            "snippet": {
              "publishedAt": "2016-03-02T02:51:37.000Z",
              "channelId": "UCAs3QvuB2QlsbmikmgeYlkg",
              "title": "ERNESTO GONZALEZ & JOSE BAZ / I WILL SURVIVE CHILL LOUNGE",
              "description": "2015 ERNESTO GONZALEZ & JOSE BAZ BARCELONA CHILL LOUNGE.",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/UtfJuW74o3U/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/UtfJuW74o3U/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/UtfJuW74o3U/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Ernesto Gonzalez",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Darroo - Space Mission",
        "artist": "Darroo",
        "music": "Space Mission",
        "label": "New Plannet",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14563449.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/FydQbsHFMB7_UisG8xjC8ejVFFU\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 17,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/MjcW-tuXRePh7l0Jjsfp2tYdOns\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "dvQYrSkokpM"
            },
            "snippet": {
              "publishedAt": "2012-01-30T04:12:05.000Z",
              "channelId": "UCTImJKG13NieAX66D8C7zIg",
              "title": "Dj Darroo - Space Mission (Original Mix) [HD Quality Version]",
              "description": "",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/dvQYrSkokpM/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/dvQYrSkokpM/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/dvQYrSkokpM/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "djdarroo",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Zatox - God Complex",
        "artist": "Zatox",
        "music": "God Complex",
        "label": "WE R",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14751159.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/0wdXv7BZ36nqqU2L0lpNy86S1LY\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 2522,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/yUv175Cm2xuBlqRgKJ1IB7FTDd0\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "rJKmQoaXL-s"
            },
            "snippet": {
              "publishedAt": "2016-02-04T15:02:05.000Z",
              "channelId": "UCWFjJaI19jDrzYgwKRfbr0w",
              "title": "Brennan Heart & Zatox - God Complex (Official Video)",
              "description": "The official video for 'God Complex' by Brennan Heart & Zatox Spotify: https://wermusic.lnk.to/GodComplexYo Soon out on WE R Music Edited by Vlad Nikolaev/ ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/rJKmQoaXL-s/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/rJKmQoaXL-s/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/rJKmQoaXL-s/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Brennan Heart",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Scott Attrill - Transformer",
        "artist": "Scott Attrill",
        "music": "Transformer",
        "label": "Traffic Records",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14563202.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/1nTwkHCwb6WX4QOOa-8Jp6q_0cI\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 23,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/y5DTUwefN-mmnruSEbTVM662-8Q\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "St7Le_cvr4Y"
            },
            "snippet": {
              "publishedAt": "2016-11-07T07:43:08.000Z",
              "channelId": "UCMdlHGWdSdfhNoEKDdDfCJw",
              "title": "Scott Attrill - Transformer (Original Mix) [Traffic Records]",
              "description": "http://www.toolboxdigitalshop.com/hard-dance/scott-attrill-transformer-original-mix-traffic-records.html To buy a full length 320kbps MP3 or WAV click the link ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/St7Le_cvr4Y/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/St7Le_cvr4Y/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/St7Le_cvr4Y/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Toolbox Digital Shop",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Forbidden Friends - Blunt Punt",
        "artist": "Forbidden Friends",
        "music": "Blunt Punt",
        "label": "Toolbox Recordings",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14563522.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/nsH_vi-AKFIxshGYHxX31dcjz-s\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 5,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/x0grKOOGplmGg8bW2Q7ICthduSE\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "8HG_pRf_tFk"
            },
            "snippet": {
              "publishedAt": "2016-10-05T15:38:33.000Z",
              "channelId": "UCMdlHGWdSdfhNoEKDdDfCJw",
              "title": "Forbidden Friends - Blunt Punt [Toolbox Recordings]",
              "description": "http://www.toolboxdigitalshop.com/exclusive/forbidden-friends-blunt-punt-toolbox-recordings.html To buy a full length 320kbps MP3 or WAV click the link above:",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/8HG_pRf_tFk/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/8HG_pRf_tFk/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/8HG_pRf_tFk/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Toolbox Digital Shop",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Jared Moreno - Chameleon",
        "artist": "Jared Moreno",
        "music": "Chameleon",
        "label": "Different Record",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14768886.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/RV8Cr1iW1LL9pXnxJ6wWjtampDI\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 21,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/C2gUtcs026YxdyX3NxvxxXQnWWA\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "M0yAz1AXpuw"
            },
            "snippet": {
              "publishedAt": "2016-11-05T15:04:34.000Z",
              "channelId": "UC7jG4DCITqSBk8l9vIGlMqw",
              "title": "Jared Moreno - Chameleon",
              "description": "Hey! My new single #Chameleon is out now! Free Download: https://fanlink.to/chamdl --- Support on iTunes: https://fanlink.to/chamtns Support on Beatport: ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/M0yAz1AXpuw/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/M0yAz1AXpuw/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/M0yAz1AXpuw/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Jared Moreno",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Marc Lewis - Boulder Holder",
        "artist": "Marc Lewis",
        "music": "Boulder Holder",
        "label": "Fireball Recordings",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14556367.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/Dg64yVq22n2-L63eaS1QcW3qc8U\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 8,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/eUKbqSJbrTSBYTnN8bNtWjBos5c\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "0o1K3Xej2Qo"
            },
            "snippet": {
              "publishedAt": "2016-10-07T06:58:33.000Z",
              "channelId": "UCMdlHGWdSdfhNoEKDdDfCJw",
              "title": "Marc Lewis - Boulder Holder [Fireball]",
              "description": "http://www.toolboxdigitalshop.com/exclusive/marc-lewis-boulder-holder-fireball.html To buy a full length 320kbps MP3 or WAV click the link above:",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/0o1K3Xej2Qo/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/0o1K3Xej2Qo/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/0o1K3Xej2Qo/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Toolbox Digital Shop",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Giuliano Aguero - Close Your Eyes",
        "artist": "Giuliano Aguero",
        "music": "Close Your Eyes",
        "label": "Qubex Music",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14648689.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/6QEeD7CdcJmmnWJnk8hy4QEhm_8\"",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 1,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/BB_IvBfc-CuTvVSu22t8m_1G5P4\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "bx6xJXy0kFw"
            },
            "snippet": {
              "publishedAt": "2016-09-28T15:31:55.000Z",
              "channelId": "UCg6lytNvAG9taS0sSM7eJzw",
              "title": "Giuliano Agüero • Close Your Eyes | Qubex Music Release ©",
              "description": "Follow Qubex Music Facebook:https://www.facebook.com/QubexMusic YouTube: https://www.youtube.com/c/qubexmusic SoundCloud: ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/bx6xJXy0kFw/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/bx6xJXy0kFw/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/bx6xJXy0kFw/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Qubex Music",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Brian NRG - Fighting To Survive",
        "artist": "Brian NRG",
        "music": "Fighting To Survive",
        "label": "Kickdrum Records",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14557563.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/nRkf-Kh3iWTPC85_FbbjmaGQiE4\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 44,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/aD2PvhbmMgMNAfDkM8w-eVT0E-s\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "PM2gdmSt_bE"
            },
            "snippet": {
              "publishedAt": "2016-11-04T10:25:10.000Z",
              "channelId": "UCt8U-xi4LQFLydEtaSQJDZg",
              "title": "Brian NRG   Fighting To Survive",
              "description": "Title: Brian NRG - Fighting To Survive Label: Kickdrum Records Catnr.: KDR003 Release date: 04-11-2016.",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/PM2gdmSt_bE/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/PM2gdmSt_bE/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/PM2gdmSt_bE/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Brian NRG",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Cupra - Control",
        "artist": "Cupra",
        "music": "Control",
        "label": "Slingshot Recordings",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14713028.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/DKpX0qcSjLF43RP44I2Ygs3rXj0\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 10,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/4i71BqckRmKA3x53-xRKVQ1zrdc\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "lcYn-iLIJtA"
            },
            "snippet": {
              "publishedAt": "2016-07-27T09:53:54.000Z",
              "channelId": "UCMdlHGWdSdfhNoEKDdDfCJw",
              "title": "Cupra - Control [Slingshot]",
              "description": "",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/lcYn-iLIJtA/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/lcYn-iLIJtA/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/lcYn-iLIJtA/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Toolbox Digital Shop",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Authentic - Years From Now",
        "artist": "Authentic",
        "music": "Years From Now",
        "label": "X-Bone",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14710935.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/5FI-4VaJ1j1cTh7m54RDnuImtjE\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 2290,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/sCIlttjoRYFyU6dOYhyCLrgMcmA\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "URe-pdUfVjY"
            },
            "snippet": {
              "publishedAt": "2016-11-05T16:25:48.000Z",
              "channelId": "UC6murUWtqOwnTL68pwjoGjQ",
              "title": "Authentic - Years From Now [HQ Original]",
              "description": "EuphoricHardStyleZ YouTube Channel. ::.:.. .If you like my uploadz please subscribe to my channels: EuphoricHardStyleZ (The latest & greatest tracks & EPs) ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/URe-pdUfVjY/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/URe-pdUfVjY/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/URe-pdUfVjY/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "EuphoricHardStyleZ",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Vane - Unshakable EP (The Remixes)",
        "artist": "Vane",
        "music": "Unshakable EP (The Remixes)",
        "label": "Loud & Lucky Recordings",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14602837.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/gohTYN2kGjwAg8Y6M2uC46Gj1tc\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 101,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/Sf2uK5BljLuStZk3wtYZ7M21nH0\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "Cx08wCO1WdU"
            },
            "snippet": {
              "publishedAt": "2016-10-28T09:47:34.000Z",
              "channelId": "UCLhZZnG0lGjqMs5jU1XUnyg",
              "title": "Alari & Vane - Unshakable EP (The Remixes) // PREVIEWS //",
              "description": "AVAILABLE ON: iTunes: http://apple.co/2eVCqBD Amazon: http://amzn.to/2eZQlZq Spotify: http://spoti.fi/2f6riSc JOIN US: Facebook: ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/Cx08wCO1WdU/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/Cx08wCO1WdU/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/Cx08wCO1WdU/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "LUCKY MUSIC TV",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Madison - Whip It Away",
        "artist": "Madison",
        "music": "Whip It Away",
        "label": "Central Stage Of Music",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14671117.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/nL7Uq3IPN9KYLUHcy7hARu6MFCA\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 296,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/_8zlgrxhShxCE_7owQOqNsiVxDs\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "gwJIko9AglY"
            },
            "snippet": {
              "publishedAt": "2015-12-13T18:56:10.000Z",
              "channelId": "UCoeOr8gG7mslhfX1gkKM_bg",
              "title": "Madison - Whip It Away (Cueboy & Tribune Remix) [TechnoBase.FM Vol. 12]",
              "description": "Follow Central Stage of Music Website: http://www.central-stage.com Facebook: http://www.facebook.com/centralstageofmusic Twitter: ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/gwJIko9AglY/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/gwJIko9AglY/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/gwJIko9AglY/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Central Stage of Music",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Requiem - Smack Ya Face",
        "artist": "Requiem",
        "music": "Smack Ya Face",
        "label": "Fusion Records",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14641463.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/bsYH1KjZRWRNlyw1F0XePg1EGfk\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 2482,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/H8KC3eQGCspHKbOdPVQgMGyZL_Q\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "8k6n7gWki2w"
            },
            "snippet": {
              "publishedAt": "2016-10-18T17:00:00.000Z",
              "channelId": "UCz0sA3ZHUQYRNGGfQBAu6Jg",
              "title": "Requiem - Smack Ya Face [Fusion 318]",
              "description": "If you can't handle this much raw power, it might leave a mark on your face. Release date: 3 November Requiem Social Media Facebook: ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/8k6n7gWki2w/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/8k6n7gWki2w/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/8k6n7gWki2w/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Fusion Records",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "The Melodyst - Wrong MF",
        "artist": "The Melodyst",
        "music": "Wrong MF",
        "label": "Traxtorm Records",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14750270.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/H4TeAhal_UW7bZO25ma3kbE-oiw\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 525,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/9_GLLPVSCphKzGXmy8YUsfJNsrk\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "AAKPXQD2zLA"
            },
            "snippet": {
              "publishedAt": "2016-11-03T09:36:16.000Z",
              "channelId": "UCrXGBKTvYIIriMzuBrl_GPA",
              "title": "The Melodyst - Wrong MF - Traxtorm 0175 [HARDCORE]",
              "description": "The Melodyst - Wrong MF - Traxtorm 0175 - 175 BPM Download: http://bit.ly/WrongMF-DL | Stream: http://bit.ly/WrongMF-ST Official release: ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/AAKPXQD2zLA/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/AAKPXQD2zLA/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/AAKPXQD2zLA/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Traxtorm Records",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Equalize - Synergy",
        "artist": "Equalize",
        "music": "Synergy",
        "label": "HBR Red",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14684807.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/DRMb-rd09PEeeA2os659infycfg\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 115,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/jNhyNuq-y0t_3sBdWQZPMQFkL04\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "z8O8-g5TpDE"
            },
            "snippet": {
              "publishedAt": "2016-10-21T15:28:51.000Z",
              "channelId": "UCTf4IQV-dCevezVc2F1S2VQ",
              "title": "Equalize - Synergy",
              "description": "Artist : Equalize Title : Synergy Label : Hexablast Records Catalogue : HBR052 Format : Digital Release Date : 03/11/2016 Follow Equalize : Facebook: ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/z8O8-g5TpDE/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/z8O8-g5TpDE/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/z8O8-g5TpDE/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Hexablast",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Unresolved - This Can't Be Real",
        "artist": "Unresolved",
        "music": "This Can't Be Real",
        "label": "Anarchy",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14665611.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/7VvoQhvpzsBJjh6HZinLupy5hn0\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 420,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/8wxkP7_VJzz94Mqxs7EeFWHlVsU\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "ot8JjDqKO5I"
            },
            "snippet": {
              "publishedAt": "2016-10-19T17:00:04.000Z",
              "channelId": "UCt8hUmML7zjM6effdZ3Ip5A",
              "title": "Digital Mindz & Unresolved - This Can't Be Real (Official HQ Preview)",
              "description": "STREAM: http://Any.lnk.to/ThisCantBeRealYL ◉ Subscribe to Dirty Workz: http://bit.ly/DWX_Subscribe ▽Subscribe to our Spotify playlist: ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/ot8JjDqKO5I/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/ot8JjDqKO5I/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/ot8JjDqKO5I/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Dirty Workz",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Burak Harsitlioglu - When I Hold You",
        "artist": "Burak Harsitlioglu",
        "music": "When I Hold You",
        "label": "Ahura Mazda Recordings NITRO",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14547474.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/603O-8rj_f0olczf0jNV-ASpguU\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 30,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/aRusZmaEop0_fiLkV-o407dEb7Q\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "P-P02yBrJ8w"
            },
            "snippet": {
              "publishedAt": "2014-06-02T05:19:56.000Z",
              "channelId": "UCAV4jptW8ht5PAlMa9PymOA",
              "title": "Burak Harsitlioglu - When I Hold You (Original mix)",
              "description": "All rights reserved to the Producer and record labels responsible for the music in this video. If any problem occurs, please send me a message and I'll remove ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/P-P02yBrJ8w/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/P-P02yBrJ8w/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/P-P02yBrJ8w/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "sophie oak",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Oryon - Dreamer EP",
        "artist": "Oryon",
        "music": "Dreamer EP",
        "label": "Gearbox Euphoria",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14676029.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/se8naXZAHYhMjLVXmUMWKMZqk3w\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 2,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/IkTaD5gxji3zi1uhw87Ui7mFFWc\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "regmIskXF0Q"
            },
            "snippet": {
              "publishedAt": "2016-10-24T17:00:02.000Z",
              "channelId": "UCH-GFr_3jzTgx4Lvm50XD8w",
              "title": "Oryon - Dreaming [GBE022]",
              "description": "GBE022 | Oryon - Dreamer EP Release Date: 02/11/2016 ▽ OUT NOW: Spotify: http://bit.ly/GBE022SPOTIFY Hardstyle.com: ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/regmIskXF0Q/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/regmIskXF0Q/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/regmIskXF0Q/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Gearbox Digital",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Retaliation - Awoken",
        "artist": "Retaliation",
        "music": "Awoken",
        "label": "Theracords",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14655147.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/eRRJHOtTmAVhUSqkCCoryPH7N1k\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 84,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/zTr9ELdU43mPh3W6-N0C4tjx_Cs\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "h-K0Q9uReOo"
            },
            "snippet": {
              "publishedAt": "2016-09-12T16:04:23.000Z",
              "channelId": "UCLjOJihIIlPLb3XSz9bm2sQ",
              "title": "Psyched playing Retaliation - Awoken | Q-Base 2016",
              "description": "Psyched @ Q-Base 2016 (Theracords Area) [Track: Retaliation - Awoken]",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/h-K0Q9uReOo/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/h-K0Q9uReOo/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/h-K0Q9uReOo/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Aced Music",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Alpha? - Resurrection",
        "artist": "Alpha?",
        "music": "Resurrection",
        "label": "A2 Records",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14718530.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/Yk_2j5WWnYqsAFGKHWI236DSM_k\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 9,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/d_4CFohsddOMGGuDqzL7GXaF12U\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "AjRIwlkDIBQ"
            },
            "snippet": {
              "publishedAt": "2016-01-09T21:18:11.000Z",
              "channelId": "UChBt_KTCx8qwauWQBeoW6RA",
              "title": "Maxelektah - untitled mixtape",
              "description": "A little musical collection inna soundsystem style, check it iyah!! 01. Lee Scratch Perry - Hello Telephone 02. Bunnington Judah meets Jideh High - The Signs 03 ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/AjRIwlkDIBQ/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/AjRIwlkDIBQ/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/AjRIwlkDIBQ/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "irie vibrations",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "S3rl - Space-Time",
        "artist": "S3rl",
        "music": "Space-Time",
        "label": "Emfa Music",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14616481.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/u8F9v_UfDIOBftXarEFaJkCSMCE\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 9,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/eBYwbcL0zvqsd616dM42oWbXIcg\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "rr0MbAyWyq4"
            },
            "snippet": {
              "publishedAt": "2016-11-02T22:07:20.000Z",
              "channelId": "UCb6JTMjrHZCYFD9Y04CBk9g",
              "title": "Space-Time - S3RL feat Riddle Anne",
              "description": "Space-Time - S3RL feat Riddle Anne Buy direct from S3RL - https://djs3rl.com/shop/space-time/ Radio Edit - iTunes ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/rr0MbAyWyq4/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/rr0MbAyWyq4/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/rr0MbAyWyq4/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "S3RL",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Shaun Williams - Finally",
        "artist": "Shaun Williams",
        "music": "Finally",
        "label": "Flaunt",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14531851.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/OscjaJoj-f0c5_-X3-NMxrafslI\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 13,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/9NhQC4wryp0_1TyoSbZtel9Jxp4\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "4OpE4w_iPTc"
            },
            "snippet": {
              "publishedAt": "2016-11-03T09:18:22.000Z",
              "channelId": "UCMdlHGWdSdfhNoEKDdDfCJw",
              "title": "Shaun Williams - Finally (Original Mix) [Flaunt]",
              "description": "http://www.toolboxdigitalshop.com/hard-house/shaun-williams-finally-original-mix-flaunt.html To buy a full length 320kbps MP3 or WAV click the link above:",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/4OpE4w_iPTc/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/4OpE4w_iPTc/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/4OpE4w_iPTc/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Toolbox Digital Shop",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Paul Elstak - Luv U More (Da Tweekaz Remix)",
        "artist": "Paul Elstak",
        "music": "Luv U More (Da Tweekaz Remix)",
        "label": "Dirty Workz",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14599797.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/R9KP9M6nCOpD-PNZzQ-a8S-Pfbk\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 3524,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/ofZ2D7XY5LbaB7C1obJx9A7gLf8\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "F40cw9uCKj8"
            },
            "snippet": {
              "publishedAt": "2016-10-13T17:00:01.000Z",
              "channelId": "UCt8hUmML7zjM6effdZ3Ip5A",
              "title": "Paul Elstak - Luv U More (Da Tweekaz Remix) (Official Video Clip)",
              "description": "DOWNLOAD: https://DirtyWorkz.lnk.to/LuvUMoreFL ◉ Subscribe to Dirty Workz: http://bit.ly/DWX_Subscribe ▽ Subscribe to our Spotify playlist: ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/F40cw9uCKj8/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/F40cw9uCKj8/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/F40cw9uCKj8/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Dirty Workz",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Technical Difficulties - Disarm You",
        "artist": "Technical Difficulties",
        "music": "Disarm You",
        "label": "Broken Records UK",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14626406.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/Tti6bPpUuflTUtdTHFBAOCwJ3Sk\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 3,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/ZKl4cCrb003ALeNS3M-NSiD6F_g\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "IO5GwfLVEC0"
            },
            "snippet": {
              "publishedAt": "2016-11-03T16:19:58.000Z",
              "channelId": "UCKmwxckepB4Ka7JJnZURhQw",
              "title": "UK/Happy Hardcore Mix October 2016 ( mixed by DJ BaseJumper )",
              "description": "UK/Happy Hardcore Mix October 2016 ( mixed by DJ BaseJumper ) ▻ 24 hours Live !!! No Stop UK Hardcore/Happy Hardcore ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/IO5GwfLVEC0/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/IO5GwfLVEC0/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/IO5GwfLVEC0/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "BassJumper86",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Steve Hewitt - Get Afro",
        "artist": "Steve Hewitt",
        "music": "Get Afro",
        "label": "Digital Damage Recordings ",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14532719.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/BvUmkP_pmujO6f1szsIm9NTBzLI\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 19,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/oxIwU9bV9RZUcKw997XOQxSMcs8\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "LTZAiCeglaw"
            },
            "snippet": {
              "publishedAt": "2016-09-22T06:42:29.000Z",
              "channelId": "UCMdlHGWdSdfhNoEKDdDfCJw",
              "title": "Jay Middleton, Steve Hewitt - Get Afro [Digital Damage Recordings]",
              "description": "",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/LTZAiCeglaw/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/LTZAiCeglaw/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/LTZAiCeglaw/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Toolbox Digital Shop",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Betavoice - Time Space",
        "artist": "Betavoice",
        "music": "Time Space",
        "label": "X-Bone",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14710408.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/hMe8JHbQszCzgaXEodtTU2su32o\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 124,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/X5X_niEPqwtX1uUp6rKbAzr2p50\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "1b8qQh0PvHE"
            },
            "snippet": {
              "publishedAt": "2016-10-24T07:17:51.000Z",
              "channelId": "UC9PKCJ2kJEU-kR85ZyaN46w",
              "title": "Betavoice - Time Space (#XBONE136)",
              "description": "Connect: www.facebook.com/Betavoice www.soundcloud.com/betavoice www.twitter.com/betavoice http://www.xbone.nl/ http://www.xbone.nl/facebook ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/1b8qQh0PvHE/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/1b8qQh0PvHE/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/1b8qQh0PvHE/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "X-BONE",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Andres Ballesteros - Running The Night",
        "artist": "Andres Ballesteros",
        "music": "Running The Night",
        "label": "DNZ Records",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14645616.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/0H0b7_9z6TbhWWuAOPLFIzlXAEI\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 16,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/8irsP3gCN_IY5MmC1Tlh_HMv_ig\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "O3YIfvKz290"
            },
            "snippet": {
              "publishedAt": "2016-11-01T09:38:39.000Z",
              "channelId": "UC_00q1mMk0DshxehYSLjbFA",
              "title": "BailaConmigo RadioShow Número Uno   Andrés Ballesteros   Running The Night",
              "description": "Número uno BailaConmigo RadioShow: FanPage Facebook: BailaConmigoRadio Twitter: @DjMuela & @PakoreDj.",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/O3YIfvKz290/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/O3YIfvKz290/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/O3YIfvKz290/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Dj Muela",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "MKN - Transcendence",
        "artist": "MKN",
        "music": "Transcendence",
        "label": "VNTG Records",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14606123.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/imo67CspXkCG_5738SJeHku3Yrc\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 121,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/tiB_5I1P1a65J5nRVtDWFsK_j7o\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "dYJ09kEF5n0"
            },
            "snippet": {
              "publishedAt": "2016-10-19T06:47:16.000Z",
              "channelId": "UCf_eU67i01jxPCiD25_WM3g",
              "title": "Synthsoldier & MKN - Transcendence (Official HQ Preview)",
              "description": "Synthsoldier & MKN - Transcendence Release Date: October 31 Follow Synthsoldier: Soundcloud: soundcloud.com/synthsoldier Facebook: ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/dYJ09kEF5n0/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/dYJ09kEF5n0/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/dYJ09kEF5n0/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "VNTG Records",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "D-Crypt - Rock The Club",
        "artist": "D-Crypt",
        "music": "Rock The Club",
        "label": "Prospectz",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14542894.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/NJ0qQSlXfKEOiftQopwf6pYxIoc\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 14,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/rjvCwYOikfkHgEr7lL0YXsEmePY\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "t__IUk2egWs"
            },
            "snippet": {
              "publishedAt": "2016-09-19T18:00:03.000Z",
              "channelId": "UC5lllH41JkvfPq5nZ2Y5S5A",
              "title": "PPR007 D-Crypt - Rock The Club",
              "description": "Holiday season is over and we are back with a fresh melodic track by our man D-Crypt! Get ready and love it! Don't forget to subscribe to our channel! And like ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/t__IUk2egWs/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/t__IUk2egWs/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/t__IUk2egWs/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Prospectz Records",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Stuback - Time of Rush",
        "artist": "Stuback",
        "music": "Time of Rush",
        "label": "Dutch Master Works",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14599809.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/9Zga2uKxlWDoI_OJjjwObSFpqIo\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 80,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/qhNH1TiSp8osZNIoXyDR-va8P0c\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "YdwwJF1VR50"
            },
            "snippet": {
              "publishedAt": "2016-10-31T15:56:55.000Z",
              "channelId": "UC7oaGfLh1926Xufj0R4GZZQ",
              "title": "Stuback - Time of Rush (Original Mix)",
              "description": "",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/YdwwJF1VR50/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/YdwwJF1VR50/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/YdwwJF1VR50/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Adasko DH",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Requiem - Trevor",
        "artist": "Requiem",
        "music": "Trevor",
        "label": "Fusion Records",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14641512.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/cMOg3VCo0-8Ry7j_hS9FG2aSeQs\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 1189,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/h5sjBtow6p4xVEUY2VIRUAvpvTg\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "C4MsCfx-8sk"
            },
            "snippet": {
              "publishedAt": "2016-10-18T17:25:19.000Z",
              "channelId": "UCz0sA3ZHUQYRNGGfQBAu6Jg",
              "title": "Requiem - Trevor [Fusion 317]",
              "description": "Sure, he might be the craziest asshole you'll ever meet, but on the inside he's just looking for love....and money...and drugs... okay you know what, just keep your ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/C4MsCfx-8sk/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/C4MsCfx-8sk/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/C4MsCfx-8sk/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Fusion Records",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Villain - Dimensions of the Underworld (Pumpkin 2016 Anthem)",
        "artist": "Villain",
        "music": "Dimensions of the Underworld (Pumpkin 2016 Anthem)",
        "label": "Dirty Workz",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14689345.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/M2AOzq6Eyq1wUuP1u5kyAfnl_Fs\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 2042,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/L3eD9pD02sNZ_ypFjwyEyw6jgYY\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "bz12An4hU-s"
            },
            "snippet": {
              "publishedAt": "2016-10-24T17:00:03.000Z",
              "channelId": "UCt8hUmML7zjM6effdZ3Ip5A",
              "title": "Wasted Penguinz x Villain - Dimensions of the Underworld (Pumpkin 2016 Anthem) (Official HQ Preview)",
              "description": "DOWNLOAD: https://DirtyWorkz.lnk.to/DimensionsOftheunderworldYL ◉ Subscribe to Dirty Workz: http://bit.ly/DWX_Subscribe ▽Subscribe to our Spotify playlist: ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/bz12An4hU-s/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/bz12An4hU-s/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/bz12An4hU-s/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Dirty Workz",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "50 Hz - By Your Side",
        "artist": "50 Hz",
        "music": "By Your Side",
        "label": "Derailed Traxx (Be Yourself Music)",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14616030.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/yIsQs1DxVvqM23dME1tja9NYD0Q\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 124,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/ISEUx6dEkD8726gGg8mqrZDDnFo\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "GTuLqyQcpjs"
            },
            "snippet": {
              "publishedAt": "2016-10-24T14:00:09.000Z",
              "channelId": "UCsyhczBBmtcBKwVqCLko8Ig",
              "title": "50 Hz - By Your Side [DT025]",
              "description": "Buy or stream: http://listento.derailedtraxx.com/byyoursideYo Check our spotify playlist The Sound of Hardstyle: http://listento.derailedtraxx.com/TSOHYo With ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/GTuLqyQcpjs/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/GTuLqyQcpjs/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/GTuLqyQcpjs/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Derailed Traxx",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Atmozfears - What About Us",
        "artist": "Atmozfears",
        "music": "What About Us",
        "label": "Scantraxx Recordz",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14710681.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/qr-O3A3i_k1E8ir7Aa-pNMJ-vPg\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 4097,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/iYPVFp_P786k8zxcn-lzV-Xj-aQ\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "0-zfqC6htLs"
            },
            "snippet": {
              "publishedAt": "2016-10-19T15:59:56.000Z",
              "channelId": "UC8tJW7k6rhRLqX-J1bKw6Rg",
              "title": "Audiotricz & Atmozfears - What About Us (Official Videoclip)",
              "description": "Subscribe for more music at http://scantraxx.com/youtube Download/stream 'What About Us' at https://scan.lnk.to/220YD Every time melodic geniuses Audiotricz ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/0-zfqC6htLs/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/0-zfqC6htLs/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/0-zfqC6htLs/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Scantraxx Recordz",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    },
    {
      "info": {
        "title": "Masters Of Chaos - Death Wish",
        "artist": "Masters Of Chaos",
        "music": "Death Wish",
        "label": "Atmosphere Recordings:UK",
        "image": "https://geo-media.beatport.com/image_size_hq/500x500/14557183.jpg",
        "download": ""
      },
      "data": {
        "kind": "youtube#searchListResponse",
        "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/kp0u_7wy6flC-_Ul8x3t-59rsoo\"",
        "nextPageToken": "CAEQAA",
        "regionCode": "BR",
        "pageInfo": {
          "totalResults": 22,
          "resultsPerPage": 1
        },
        "items": [
          {
            "kind": "youtube#searchResult",
            "etag": "\"sZ5p5Mo8dPpfIzLYQBF8QIQJym0/dPCg6LL2EhLaqvFpAJBl1kXnITc\"",
            "id": {
              "kind": "youtube#video",
              "videoId": "Gy2A_MF8lVk"
            },
            "snippet": {
              "publishedAt": "2016-10-31T08:15:02.000Z",
              "channelId": "UCMdlHGWdSdfhNoEKDdDfCJw",
              "title": "Masters Of Chaos - Death Wish (Original Mix) [Atmosphere Recordings:UK]",
              "description": "http://www.toolboxdigitalshop.com/hard-dance/masters-of-chaos-death-wish-original-mix-atmosphere-recordings-uk.html To buy a full length 320kbps MP3 or ...",
              "thumbnails": {
                "default": {
                  "url": "https://i.ytimg.com/vi/Gy2A_MF8lVk/default.jpg",
                  "width": 120,
                  "height": 90
                },
                "medium": {
                  "url": "https://i.ytimg.com/vi/Gy2A_MF8lVk/mqdefault.jpg",
                  "width": 320,
                  "height": 180
                },
                "high": {
                  "url": "https://i.ytimg.com/vi/Gy2A_MF8lVk/hqdefault.jpg",
                  "width": 480,
                  "height": 360
                }
              },
              "channelTitle": "Toolbox Digital Shop",
              "liveBroadcastContent": "none"
            }
          }
        ]
      }
    }
  ],
    podcasts: {
    "paging": {
      "previous": "https://api.mixcloud.com/djadriano/cloudcasts/?limit=20&since=2016-10-30+01%3A04%3A19",
      "next": "https://api.mixcloud.com/djadriano/cloudcasts/?limit=20&until=2016-02-05+00%3A09%3A41"
    },
    "data": [
      {
        "tags": [
          {
            "url": "https://www.mixcloud.com/discover/hardstyle/",
            "name": "Hardstyle",
            "key": "/discover/hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/hardcore/",
            "name": "Hardcore",
            "key": "/discover/hardcore/"
          }
        ],
        "play_count": 36,
        "user": {
          "url": "https://www.mixcloud.com/djadriano/",
          "username": "djadriano",
          "name": "DJ Adriano Fernandes",
          "key": "/djadriano/",
          "pictures": {
            "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg"
          }
        },
        "key": "/djadriano/dj-adriano-fernandes-hands-up-in-the-air-57/",
        "created_time": "2016-10-30T01:04:19Z",
        "audio_length": 3429,
        "slug": "dj-adriano-fernandes-hands-up-in-the-air-57",
        "favorite_count": 3,
        "listener_count": 2,
        "name": "DJ Adriano Fernandes - Hands Up In the Air 57",
        "url": "https://www.mixcloud.com/djadriano/dj-adriano-fernandes-hands-up-in-the-air-57/",
        "pictures": {
          "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/extaudio/3/e/3/8/4fd8-68fe-4f2c-aa31-aba78c20ea96",
          "768wx768h": "https://thumbnailer.mixcloud.com/unsafe/768x768/extaudio/3/e/3/8/4fd8-68fe-4f2c-aa31-aba78c20ea96",
          "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/extaudio/3/e/3/8/4fd8-68fe-4f2c-aa31-aba78c20ea96",
          "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/3/e/3/8/4fd8-68fe-4f2c-aa31-aba78c20ea96",
          "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/extaudio/3/e/3/8/4fd8-68fe-4f2c-aa31-aba78c20ea96",
          "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/extaudio/3/e/3/8/4fd8-68fe-4f2c-aa31-aba78c20ea96",
          "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/extaudio/3/e/3/8/4fd8-68fe-4f2c-aa31-aba78c20ea96",
          "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/extaudio/3/e/3/8/4fd8-68fe-4f2c-aa31-aba78c20ea96",
          "1024wx1024h": "https://thumbnailer.mixcloud.com/unsafe/1024x1024/extaudio/3/e/3/8/4fd8-68fe-4f2c-aa31-aba78c20ea96",
          "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/extaudio/3/e/3/8/4fd8-68fe-4f2c-aa31-aba78c20ea96"
        },
        "repost_count": 1,
        "updated_time": "2016-10-30T01:04:06Z",
        "comment_count": 0
      },
      {
        "tags": [
          {
            "url": "https://www.mixcloud.com/discover/hardstyle/",
            "name": "Hardstyle",
            "key": "/discover/hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/hardcore/",
            "name": "Hardcore",
            "key": "/discover/hardcore/"
          },
          {
            "url": "https://www.mixcloud.com/discover/electronic-music/",
            "name": "Electronic music",
            "key": "/discover/electronic-music/"
          }
        ],
        "play_count": 29,
        "user": {
          "url": "https://www.mixcloud.com/djadriano/",
          "username": "djadriano",
          "name": "DJ Adriano Fernandes",
          "key": "/djadriano/",
          "pictures": {
            "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg"
          }
        },
        "key": "/djadriano/dj-adriano-fernandes-hands-up-in-the-air-56/",
        "created_time": "2016-10-20T02:04:21Z",
        "audio_length": 3591,
        "slug": "dj-adriano-fernandes-hands-up-in-the-air-56",
        "favorite_count": 4,
        "listener_count": 4,
        "name": "DJ Adriano Fernandes - Hands Up In the Air 56",
        "url": "https://www.mixcloud.com/djadriano/dj-adriano-fernandes-hands-up-in-the-air-56/",
        "pictures": {
          "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/extaudio/a/8/3/8/c22e-ba06-460c-9a03-69acafce7776",
          "768wx768h": "https://thumbnailer.mixcloud.com/unsafe/768x768/extaudio/a/8/3/8/c22e-ba06-460c-9a03-69acafce7776",
          "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/extaudio/a/8/3/8/c22e-ba06-460c-9a03-69acafce7776",
          "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/a/8/3/8/c22e-ba06-460c-9a03-69acafce7776",
          "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/extaudio/a/8/3/8/c22e-ba06-460c-9a03-69acafce7776",
          "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/extaudio/a/8/3/8/c22e-ba06-460c-9a03-69acafce7776",
          "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/extaudio/a/8/3/8/c22e-ba06-460c-9a03-69acafce7776",
          "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/extaudio/a/8/3/8/c22e-ba06-460c-9a03-69acafce7776",
          "1024wx1024h": "https://thumbnailer.mixcloud.com/unsafe/1024x1024/extaudio/a/8/3/8/c22e-ba06-460c-9a03-69acafce7776",
          "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/extaudio/a/8/3/8/c22e-ba06-460c-9a03-69acafce7776"
        },
        "repost_count": 1,
        "updated_time": "2016-10-20T02:04:06Z",
        "comment_count": 0
      },
      {
        "tags": [
          {
            "url": "https://www.mixcloud.com/discover/hardstyle/",
            "name": "Hardstyle",
            "key": "/discover/hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/hardcore/",
            "name": "Hardcore",
            "key": "/discover/hardcore/"
          }
        ],
        "play_count": 61,
        "user": {
          "url": "https://www.mixcloud.com/djadriano/",
          "username": "djadriano",
          "name": "DJ Adriano Fernandes",
          "key": "/djadriano/",
          "pictures": {
            "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg"
          }
        },
        "key": "/djadriano/dj-adriano-fernandes-hands-up-in-the-air-55/",
        "created_time": "2016-09-18T21:58:39Z",
        "audio_length": 3949,
        "slug": "dj-adriano-fernandes-hands-up-in-the-air-55",
        "favorite_count": 0,
        "listener_count": 5,
        "name": "DJ Adriano Fernandes - Hands Up In the Air 55",
        "url": "https://www.mixcloud.com/djadriano/dj-adriano-fernandes-hands-up-in-the-air-55/",
        "pictures": {
          "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/extaudio/5/b/c/3/b6e1-a6d7-4aa6-8670-82661a7f5143",
          "768wx768h": "https://thumbnailer.mixcloud.com/unsafe/768x768/extaudio/5/b/c/3/b6e1-a6d7-4aa6-8670-82661a7f5143",
          "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/extaudio/5/b/c/3/b6e1-a6d7-4aa6-8670-82661a7f5143",
          "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/5/b/c/3/b6e1-a6d7-4aa6-8670-82661a7f5143",
          "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/extaudio/5/b/c/3/b6e1-a6d7-4aa6-8670-82661a7f5143",
          "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/extaudio/5/b/c/3/b6e1-a6d7-4aa6-8670-82661a7f5143",
          "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/extaudio/5/b/c/3/b6e1-a6d7-4aa6-8670-82661a7f5143",
          "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/extaudio/5/b/c/3/b6e1-a6d7-4aa6-8670-82661a7f5143",
          "1024wx1024h": "https://thumbnailer.mixcloud.com/unsafe/1024x1024/extaudio/5/b/c/3/b6e1-a6d7-4aa6-8670-82661a7f5143",
          "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/extaudio/5/b/c/3/b6e1-a6d7-4aa6-8670-82661a7f5143"
        },
        "repost_count": 0,
        "updated_time": "2016-09-18T21:56:44Z",
        "comment_count": 0
      },
      {
        "tags": [
          {
            "url": "https://www.mixcloud.com/discover/hardstyle/",
            "name": "Hardstyle",
            "key": "/discover/hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/hardcore/",
            "name": "Hardcore",
            "key": "/discover/hardcore/"
          },
          {
            "url": "https://www.mixcloud.com/discover/electronic-music/",
            "name": "Electronic music",
            "key": "/discover/electronic-music/"
          }
        ],
        "play_count": 136,
        "user": {
          "url": "https://www.mixcloud.com/djadriano/",
          "username": "djadriano",
          "name": "DJ Adriano Fernandes",
          "key": "/djadriano/",
          "pictures": {
            "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg"
          }
        },
        "key": "/djadriano/dj-adriano-fernandes-hard-fall-dj-contest/",
        "created_time": "2016-08-24T02:52:55Z",
        "audio_length": 2695,
        "slug": "dj-adriano-fernandes-hard-fall-dj-contest",
        "favorite_count": 3,
        "listener_count": 18,
        "name": "DJ Adriano Fernandes - Hard Fall Dj Contest",
        "url": "https://www.mixcloud.com/djadriano/dj-adriano-fernandes-hard-fall-dj-contest/",
        "pictures": {
          "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/extaudio/e/b/b/3/444e-278d-415d-bdaf-6237b2a81740",
          "768wx768h": "https://thumbnailer.mixcloud.com/unsafe/768x768/extaudio/e/b/b/3/444e-278d-415d-bdaf-6237b2a81740",
          "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/extaudio/e/b/b/3/444e-278d-415d-bdaf-6237b2a81740",
          "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/e/b/b/3/444e-278d-415d-bdaf-6237b2a81740",
          "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/extaudio/e/b/b/3/444e-278d-415d-bdaf-6237b2a81740",
          "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/extaudio/e/b/b/3/444e-278d-415d-bdaf-6237b2a81740",
          "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/extaudio/e/b/b/3/444e-278d-415d-bdaf-6237b2a81740",
          "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/extaudio/e/b/b/3/444e-278d-415d-bdaf-6237b2a81740",
          "1024wx1024h": "https://thumbnailer.mixcloud.com/unsafe/1024x1024/extaudio/e/b/b/3/444e-278d-415d-bdaf-6237b2a81740",
          "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/extaudio/e/b/b/3/444e-278d-415d-bdaf-6237b2a81740"
        },
        "repost_count": 3,
        "updated_time": "2016-08-24T02:52:24Z",
        "comment_count": 2
      },
      {
        "tags": [
          {
            "url": "https://www.mixcloud.com/discover/hardstyle/",
            "name": "Hardstyle",
            "key": "/discover/hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/hardcore/",
            "name": "Hardcore",
            "key": "/discover/hardcore/"
          }
        ],
        "play_count": 67,
        "user": {
          "url": "https://www.mixcloud.com/djadriano/",
          "username": "djadriano",
          "name": "DJ Adriano Fernandes",
          "key": "/djadriano/",
          "pictures": {
            "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg"
          }
        },
        "key": "/djadriano/dj-adriano-fernandes-hands-up-in-the-air-54/",
        "created_time": "2016-07-10T00:22:56Z",
        "audio_length": 3610,
        "slug": "dj-adriano-fernandes-hands-up-in-the-air-54",
        "favorite_count": 0,
        "listener_count": 1,
        "name": "DJ Adriano Fernandes - Hands Up In the Air 54",
        "url": "https://www.mixcloud.com/djadriano/dj-adriano-fernandes-hands-up-in-the-air-54/",
        "pictures": {
          "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/extaudio/e/d/6/b/e9ec-8237-4c10-a52e-88982ef0a7cf",
          "768wx768h": "https://thumbnailer.mixcloud.com/unsafe/768x768/extaudio/e/d/6/b/e9ec-8237-4c10-a52e-88982ef0a7cf",
          "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/extaudio/e/d/6/b/e9ec-8237-4c10-a52e-88982ef0a7cf",
          "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/e/d/6/b/e9ec-8237-4c10-a52e-88982ef0a7cf",
          "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/extaudio/e/d/6/b/e9ec-8237-4c10-a52e-88982ef0a7cf",
          "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/extaudio/e/d/6/b/e9ec-8237-4c10-a52e-88982ef0a7cf",
          "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/extaudio/e/d/6/b/e9ec-8237-4c10-a52e-88982ef0a7cf",
          "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/extaudio/e/d/6/b/e9ec-8237-4c10-a52e-88982ef0a7cf",
          "1024wx1024h": "https://thumbnailer.mixcloud.com/unsafe/1024x1024/extaudio/e/d/6/b/e9ec-8237-4c10-a52e-88982ef0a7cf",
          "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/extaudio/e/d/6/b/e9ec-8237-4c10-a52e-88982ef0a7cf"
        },
        "repost_count": 0,
        "updated_time": "2016-07-10T00:22:45Z",
        "comment_count": 0
      },
      {
        "tags": [
          {
            "url": "https://www.mixcloud.com/discover/hardstyle/",
            "name": "Hardstyle",
            "key": "/discover/hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/hardcore/",
            "name": "Hardcore",
            "key": "/discover/hardcore/"
          },
          {
            "url": "https://www.mixcloud.com/discover/electronic-music/",
            "name": "Electronic music",
            "key": "/discover/electronic-music/"
          },
          {
            "url": "https://www.mixcloud.com/discover/hard-dance/",
            "name": "Hard Dance",
            "key": "/discover/hard-dance/"
          }
        ],
        "play_count": 66,
        "user": {
          "url": "https://www.mixcloud.com/djadriano/",
          "username": "djadriano",
          "name": "DJ Adriano Fernandes",
          "key": "/djadriano/",
          "pictures": {
            "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg"
          }
        },
        "key": "/djadriano/dj-adriano-fernandes-hands-up-in-the-air-53/",
        "created_time": "2016-07-02T21:15:11Z",
        "audio_length": 3361,
        "slug": "dj-adriano-fernandes-hands-up-in-the-air-53",
        "favorite_count": 2,
        "listener_count": 8,
        "name": "DJ Adriano Fernandes - Hands Up In the Air 53",
        "url": "https://www.mixcloud.com/djadriano/dj-adriano-fernandes-hands-up-in-the-air-53/",
        "pictures": {
          "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/extaudio/3/8/0/c/b9f9-3ac5-4ee8-b2dd-8a4a6d9a6237",
          "768wx768h": "https://thumbnailer.mixcloud.com/unsafe/768x768/extaudio/3/8/0/c/b9f9-3ac5-4ee8-b2dd-8a4a6d9a6237",
          "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/extaudio/3/8/0/c/b9f9-3ac5-4ee8-b2dd-8a4a6d9a6237",
          "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/3/8/0/c/b9f9-3ac5-4ee8-b2dd-8a4a6d9a6237",
          "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/extaudio/3/8/0/c/b9f9-3ac5-4ee8-b2dd-8a4a6d9a6237",
          "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/extaudio/3/8/0/c/b9f9-3ac5-4ee8-b2dd-8a4a6d9a6237",
          "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/extaudio/3/8/0/c/b9f9-3ac5-4ee8-b2dd-8a4a6d9a6237",
          "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/extaudio/3/8/0/c/b9f9-3ac5-4ee8-b2dd-8a4a6d9a6237",
          "1024wx1024h": "https://thumbnailer.mixcloud.com/unsafe/1024x1024/extaudio/3/8/0/c/b9f9-3ac5-4ee8-b2dd-8a4a6d9a6237",
          "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/extaudio/3/8/0/c/b9f9-3ac5-4ee8-b2dd-8a4a6d9a6237"
        },
        "repost_count": 1,
        "updated_time": "2016-07-02T21:14:11Z",
        "comment_count": 0
      },
      {
        "tags": [
          {
            "url": "https://www.mixcloud.com/discover/hardstyle/",
            "name": "Hardstyle",
            "key": "/discover/hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/hardcore/",
            "name": "Hardcore",
            "key": "/discover/hardcore/"
          },
          {
            "url": "https://www.mixcloud.com/discover/electronic-music/",
            "name": "Electronic music",
            "key": "/discover/electronic-music/"
          }
        ],
        "play_count": 53,
        "user": {
          "url": "https://www.mixcloud.com/djadriano/",
          "username": "djadriano",
          "name": "DJ Adriano Fernandes",
          "key": "/djadriano/",
          "pictures": {
            "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg"
          }
        },
        "key": "/djadriano/dj-adriano-fernandes-hands-up-in-the-air-52/",
        "created_time": "2016-06-18T22:33:19Z",
        "audio_length": 3264,
        "slug": "dj-adriano-fernandes-hands-up-in-the-air-52",
        "favorite_count": 5,
        "listener_count": 5,
        "name": "DJ Adriano Fernandes - Hands Up In the Air 52",
        "url": "https://www.mixcloud.com/djadriano/dj-adriano-fernandes-hands-up-in-the-air-52/",
        "pictures": {
          "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/extaudio/0/d/6/b/08d5-02c4-4e5c-955a-3ed3b1817f2b",
          "768wx768h": "https://thumbnailer.mixcloud.com/unsafe/768x768/extaudio/0/d/6/b/08d5-02c4-4e5c-955a-3ed3b1817f2b",
          "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/extaudio/0/d/6/b/08d5-02c4-4e5c-955a-3ed3b1817f2b",
          "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/0/d/6/b/08d5-02c4-4e5c-955a-3ed3b1817f2b",
          "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/extaudio/0/d/6/b/08d5-02c4-4e5c-955a-3ed3b1817f2b",
          "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/extaudio/0/d/6/b/08d5-02c4-4e5c-955a-3ed3b1817f2b",
          "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/extaudio/0/d/6/b/08d5-02c4-4e5c-955a-3ed3b1817f2b",
          "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/extaudio/0/d/6/b/08d5-02c4-4e5c-955a-3ed3b1817f2b",
          "1024wx1024h": "https://thumbnailer.mixcloud.com/unsafe/1024x1024/extaudio/0/d/6/b/08d5-02c4-4e5c-955a-3ed3b1817f2b",
          "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/extaudio/0/d/6/b/08d5-02c4-4e5c-955a-3ed3b1817f2b"
        },
        "repost_count": 1,
        "updated_time": "2016-06-18T22:33:04Z",
        "comment_count": 0
      },
      {
        "tags": [
          {
            "url": "https://www.mixcloud.com/discover/hardstyle/",
            "name": "Hardstyle",
            "key": "/discover/hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/hardcore/",
            "name": "Hardcore",
            "key": "/discover/hardcore/"
          },
          {
            "url": "https://www.mixcloud.com/discover/raw/",
            "name": "Raw",
            "key": "/discover/raw/"
          },
          {
            "url": "https://www.mixcloud.com/discover/electronic-music/",
            "name": "Electronic music",
            "key": "/discover/electronic-music/"
          }
        ],
        "play_count": 56,
        "user": {
          "url": "https://www.mixcloud.com/djadriano/",
          "username": "djadriano",
          "name": "DJ Adriano Fernandes",
          "key": "/djadriano/",
          "pictures": {
            "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg"
          }
        },
        "key": "/djadriano/dj-adriano-fernandes-hands-up-in-the-air-51/",
        "created_time": "2016-05-28T19:41:00Z",
        "audio_length": 3572,
        "slug": "dj-adriano-fernandes-hands-up-in-the-air-51",
        "favorite_count": 0,
        "listener_count": 3,
        "name": "DJ Adriano Fernandes - Hands Up In the Air 51",
        "url": "https://www.mixcloud.com/djadriano/dj-adriano-fernandes-hands-up-in-the-air-51/",
        "pictures": {
          "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/extaudio/a/f/a/e/e080-b5f3-4a62-bbb4-ee0657c3c026",
          "768wx768h": "https://thumbnailer.mixcloud.com/unsafe/768x768/extaudio/a/f/a/e/e080-b5f3-4a62-bbb4-ee0657c3c026",
          "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/extaudio/a/f/a/e/e080-b5f3-4a62-bbb4-ee0657c3c026",
          "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/a/f/a/e/e080-b5f3-4a62-bbb4-ee0657c3c026",
          "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/extaudio/a/f/a/e/e080-b5f3-4a62-bbb4-ee0657c3c026",
          "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/extaudio/a/f/a/e/e080-b5f3-4a62-bbb4-ee0657c3c026",
          "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/extaudio/a/f/a/e/e080-b5f3-4a62-bbb4-ee0657c3c026",
          "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/extaudio/a/f/a/e/e080-b5f3-4a62-bbb4-ee0657c3c026",
          "1024wx1024h": "https://thumbnailer.mixcloud.com/unsafe/1024x1024/extaudio/a/f/a/e/e080-b5f3-4a62-bbb4-ee0657c3c026",
          "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/extaudio/a/f/a/e/e080-b5f3-4a62-bbb4-ee0657c3c026"
        },
        "repost_count": 0,
        "updated_time": "2016-05-28T19:40:48Z",
        "comment_count": 0
      },
      {
        "tags": [
          {
            "url": "https://www.mixcloud.com/discover/hardstyle/",
            "name": "Hardstyle",
            "key": "/discover/hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/hardcore/",
            "name": "Hardcore",
            "key": "/discover/hardcore/"
          }
        ],
        "play_count": 97,
        "user": {
          "url": "https://www.mixcloud.com/djadriano/",
          "username": "djadriano",
          "name": "DJ Adriano Fernandes",
          "key": "/djadriano/",
          "pictures": {
            "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg"
          }
        },
        "key": "/djadriano/hard-island-2016-wildcard-competition-by-dj-adriano-fernandes/",
        "created_time": "2016-05-20T02:15:55Z",
        "audio_length": 1791,
        "slug": "hard-island-2016-wildcard-competition-by-dj-adriano-fernandes",
        "favorite_count": 9,
        "listener_count": 5,
        "name": "Hard Island 2016 Wildcard competition by DJ Adriano Fernandes",
        "url": "https://www.mixcloud.com/djadriano/hard-island-2016-wildcard-competition-by-dj-adriano-fernandes/",
        "pictures": {
          "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/extaudio/0/3/e/f/7aac-2a9e-46bf-b018-acf68fabc49f",
          "768wx768h": "https://thumbnailer.mixcloud.com/unsafe/768x768/extaudio/0/3/e/f/7aac-2a9e-46bf-b018-acf68fabc49f",
          "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/extaudio/0/3/e/f/7aac-2a9e-46bf-b018-acf68fabc49f",
          "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/0/3/e/f/7aac-2a9e-46bf-b018-acf68fabc49f",
          "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/extaudio/0/3/e/f/7aac-2a9e-46bf-b018-acf68fabc49f",
          "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/extaudio/0/3/e/f/7aac-2a9e-46bf-b018-acf68fabc49f",
          "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/extaudio/0/3/e/f/7aac-2a9e-46bf-b018-acf68fabc49f",
          "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/extaudio/0/3/e/f/7aac-2a9e-46bf-b018-acf68fabc49f",
          "1024wx1024h": "https://thumbnailer.mixcloud.com/unsafe/1024x1024/extaudio/0/3/e/f/7aac-2a9e-46bf-b018-acf68fabc49f",
          "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/extaudio/0/3/e/f/7aac-2a9e-46bf-b018-acf68fabc49f"
        },
        "repost_count": 6,
        "updated_time": "2016-05-20T02:15:46Z",
        "comment_count": 8
      },
      {
        "tags": [
          {
            "url": "https://www.mixcloud.com/discover/hardstyle/",
            "name": "Hardstyle",
            "key": "/discover/hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/hardcore/",
            "name": "Hardcore",
            "key": "/discover/hardcore/"
          },
          {
            "url": "https://www.mixcloud.com/discover/raw/",
            "name": "Raw",
            "key": "/discover/raw/"
          },
          {
            "url": "https://www.mixcloud.com/discover/hard-dance/",
            "name": "Hard Dance",
            "key": "/discover/hard-dance/"
          }
        ],
        "play_count": 66,
        "user": {
          "url": "https://www.mixcloud.com/djadriano/",
          "username": "djadriano",
          "name": "DJ Adriano Fernandes",
          "key": "/djadriano/",
          "pictures": {
            "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg"
          }
        },
        "key": "/djadriano/dj-adriano-fernandes-hands-up-in-the-air-50/",
        "created_time": "2016-05-16T01:35:32Z",
        "audio_length": 3090,
        "slug": "dj-adriano-fernandes-hands-up-in-the-air-50",
        "favorite_count": 1,
        "listener_count": 4,
        "name": "DJ Adriano Fernandes - Hands Up In the Air 50",
        "url": "https://www.mixcloud.com/djadriano/dj-adriano-fernandes-hands-up-in-the-air-50/",
        "pictures": {
          "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/extaudio/f/9/0/7/8bd2-b864-4775-9845-d0f38de38497",
          "768wx768h": "https://thumbnailer.mixcloud.com/unsafe/768x768/extaudio/f/9/0/7/8bd2-b864-4775-9845-d0f38de38497",
          "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/extaudio/f/9/0/7/8bd2-b864-4775-9845-d0f38de38497",
          "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/f/9/0/7/8bd2-b864-4775-9845-d0f38de38497",
          "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/extaudio/f/9/0/7/8bd2-b864-4775-9845-d0f38de38497",
          "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/extaudio/f/9/0/7/8bd2-b864-4775-9845-d0f38de38497",
          "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/extaudio/f/9/0/7/8bd2-b864-4775-9845-d0f38de38497",
          "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/extaudio/f/9/0/7/8bd2-b864-4775-9845-d0f38de38497",
          "1024wx1024h": "https://thumbnailer.mixcloud.com/unsafe/1024x1024/extaudio/f/9/0/7/8bd2-b864-4775-9845-d0f38de38497",
          "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/extaudio/f/9/0/7/8bd2-b864-4775-9845-d0f38de38497"
        },
        "repost_count": 0,
        "updated_time": "2016-05-16T01:35:21Z",
        "comment_count": 0
      },
      {
        "tags": [
          {
            "url": "https://www.mixcloud.com/discover/hardstyle/",
            "name": "Hardstyle",
            "key": "/discover/hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/raw/",
            "name": "Raw",
            "key": "/discover/raw/"
          },
          {
            "url": "https://www.mixcloud.com/discover/hardcore/",
            "name": "Hardcore",
            "key": "/discover/hardcore/"
          }
        ],
        "play_count": 39,
        "user": {
          "url": "https://www.mixcloud.com/djadriano/",
          "username": "djadriano",
          "name": "DJ Adriano Fernandes",
          "key": "/djadriano/",
          "pictures": {
            "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg"
          }
        },
        "key": "/djadriano/dj-adriano-fernandes-hands-up-in-the-air-37/",
        "created_time": "2016-05-10T02:01:46Z",
        "audio_length": 3358,
        "slug": "dj-adriano-fernandes-hands-up-in-the-air-37",
        "favorite_count": 1,
        "listener_count": 10,
        "name": "DJ Adriano Fernandes - Hands Up In the Air 37",
        "url": "https://www.mixcloud.com/djadriano/dj-adriano-fernandes-hands-up-in-the-air-37/",
        "pictures": {
          "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/extaudio/5/b/e/8/744d-e3c9-499e-bc70-8ffe1c3ec049",
          "768wx768h": "https://thumbnailer.mixcloud.com/unsafe/768x768/extaudio/5/b/e/8/744d-e3c9-499e-bc70-8ffe1c3ec049",
          "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/extaudio/5/b/e/8/744d-e3c9-499e-bc70-8ffe1c3ec049",
          "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/5/b/e/8/744d-e3c9-499e-bc70-8ffe1c3ec049",
          "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/extaudio/5/b/e/8/744d-e3c9-499e-bc70-8ffe1c3ec049",
          "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/extaudio/5/b/e/8/744d-e3c9-499e-bc70-8ffe1c3ec049",
          "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/extaudio/5/b/e/8/744d-e3c9-499e-bc70-8ffe1c3ec049",
          "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/extaudio/5/b/e/8/744d-e3c9-499e-bc70-8ffe1c3ec049",
          "1024wx1024h": "https://thumbnailer.mixcloud.com/unsafe/1024x1024/extaudio/5/b/e/8/744d-e3c9-499e-bc70-8ffe1c3ec049",
          "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/extaudio/5/b/e/8/744d-e3c9-499e-bc70-8ffe1c3ec049"
        },
        "repost_count": 0,
        "updated_time": "2016-05-10T02:01:40Z",
        "comment_count": 0
      },
      {
        "tags": [
          {
            "url": "https://www.mixcloud.com/discover/hardstyle/",
            "name": "Hardstyle",
            "key": "/discover/hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/hardcore/",
            "name": "Hardcore",
            "key": "/discover/hardcore/"
          },
          {
            "url": "https://www.mixcloud.com/discover/raw/",
            "name": "Raw",
            "key": "/discover/raw/"
          }
        ],
        "play_count": 55,
        "user": {
          "url": "https://www.mixcloud.com/djadriano/",
          "username": "djadriano",
          "name": "DJ Adriano Fernandes",
          "key": "/djadriano/",
          "pictures": {
            "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg"
          }
        },
        "key": "/djadriano/dj-adriano-fernandes-hands-up-in-the-air-49/",
        "created_time": "2016-04-28T03:51:38Z",
        "audio_length": 3432,
        "slug": "dj-adriano-fernandes-hands-up-in-the-air-49",
        "favorite_count": 3,
        "listener_count": 4,
        "name": "DJ Adriano Fernandes - Hands Up In the Air 49",
        "url": "https://www.mixcloud.com/djadriano/dj-adriano-fernandes-hands-up-in-the-air-49/",
        "pictures": {
          "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/extaudio/2/c/b/b/b1c5-df46-4dfc-9d01-91d3ee8b642d",
          "768wx768h": "https://thumbnailer.mixcloud.com/unsafe/768x768/extaudio/2/c/b/b/b1c5-df46-4dfc-9d01-91d3ee8b642d",
          "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/extaudio/2/c/b/b/b1c5-df46-4dfc-9d01-91d3ee8b642d",
          "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/2/c/b/b/b1c5-df46-4dfc-9d01-91d3ee8b642d",
          "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/extaudio/2/c/b/b/b1c5-df46-4dfc-9d01-91d3ee8b642d",
          "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/extaudio/2/c/b/b/b1c5-df46-4dfc-9d01-91d3ee8b642d",
          "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/extaudio/2/c/b/b/b1c5-df46-4dfc-9d01-91d3ee8b642d",
          "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/extaudio/2/c/b/b/b1c5-df46-4dfc-9d01-91d3ee8b642d",
          "1024wx1024h": "https://thumbnailer.mixcloud.com/unsafe/1024x1024/extaudio/2/c/b/b/b1c5-df46-4dfc-9d01-91d3ee8b642d",
          "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/extaudio/2/c/b/b/b1c5-df46-4dfc-9d01-91d3ee8b642d"
        },
        "repost_count": 0,
        "updated_time": "2016-04-28T03:51:28Z",
        "comment_count": 0
      },
      {
        "tags": [
          {
            "url": "https://www.mixcloud.com/discover/hardstyle/",
            "name": "Hardstyle",
            "key": "/discover/hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/hardcore/",
            "name": "Hardcore",
            "key": "/discover/hardcore/"
          }
        ],
        "play_count": 41,
        "user": {
          "url": "https://www.mixcloud.com/djadriano/",
          "username": "djadriano",
          "name": "DJ Adriano Fernandes",
          "key": "/djadriano/",
          "pictures": {
            "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg"
          }
        },
        "key": "/djadriano/dj-adriano-fernandes-hands-up-in-the-air-48/",
        "created_time": "2016-04-16T22:31:16Z",
        "audio_length": 3475,
        "slug": "dj-adriano-fernandes-hands-up-in-the-air-48",
        "favorite_count": 1,
        "listener_count": 2,
        "name": "DJ Adriano Fernandes - Hands Up In the Air 48",
        "url": "https://www.mixcloud.com/djadriano/dj-adriano-fernandes-hands-up-in-the-air-48/",
        "pictures": {
          "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/extaudio/9/3/1/b/753d-c4c0-4a6b-a7fc-37caaccd4764.jpg",
          "768wx768h": "https://thumbnailer.mixcloud.com/unsafe/768x768/extaudio/9/3/1/b/753d-c4c0-4a6b-a7fc-37caaccd4764.jpg",
          "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/extaudio/9/3/1/b/753d-c4c0-4a6b-a7fc-37caaccd4764.jpg",
          "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/9/3/1/b/753d-c4c0-4a6b-a7fc-37caaccd4764.jpg",
          "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/extaudio/9/3/1/b/753d-c4c0-4a6b-a7fc-37caaccd4764.jpg",
          "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/extaudio/9/3/1/b/753d-c4c0-4a6b-a7fc-37caaccd4764.jpg",
          "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/extaudio/9/3/1/b/753d-c4c0-4a6b-a7fc-37caaccd4764.jpg",
          "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/extaudio/9/3/1/b/753d-c4c0-4a6b-a7fc-37caaccd4764.jpg",
          "1024wx1024h": "https://thumbnailer.mixcloud.com/unsafe/1024x1024/extaudio/9/3/1/b/753d-c4c0-4a6b-a7fc-37caaccd4764.jpg",
          "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/extaudio/9/3/1/b/753d-c4c0-4a6b-a7fc-37caaccd4764.jpg"
        },
        "repost_count": 0,
        "updated_time": "2016-04-16T22:27:09Z",
        "comment_count": 1
      },
      {
        "tags": [
          {
            "url": "https://www.mixcloud.com/discover/hardstyle/",
            "name": "Hardstyle",
            "key": "/discover/hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/scantraxx/",
            "name": "Scantraxx",
            "key": "/discover/scantraxx/"
          }
        ],
        "play_count": 101,
        "user": {
          "url": "https://www.mixcloud.com/djadriano/",
          "username": "djadriano",
          "name": "DJ Adriano Fernandes",
          "key": "/djadriano/",
          "pictures": {
            "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg"
          }
        },
        "key": "/djadriano/dj-adriano-fernandes-mix-scantraxx-dj-contest-summerfestival/",
        "created_time": "2016-04-07T03:35:18Z",
        "audio_length": 3490,
        "slug": "dj-adriano-fernandes-mix-scantraxx-dj-contest-summerfestival",
        "favorite_count": 1,
        "listener_count": 3,
        "name": "DJ Adriano Fernandes – Mix Scantraxx DJ Contest Summerfestival",
        "url": "https://www.mixcloud.com/djadriano/dj-adriano-fernandes-mix-scantraxx-dj-contest-summerfestival/",
        "pictures": {
          "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/extaudio/1/9/3/d/c45c-e3cc-4aab-b05c-505726ae3535.jpg",
          "768wx768h": "https://thumbnailer.mixcloud.com/unsafe/768x768/extaudio/1/9/3/d/c45c-e3cc-4aab-b05c-505726ae3535.jpg",
          "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/extaudio/1/9/3/d/c45c-e3cc-4aab-b05c-505726ae3535.jpg",
          "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/1/9/3/d/c45c-e3cc-4aab-b05c-505726ae3535.jpg",
          "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/extaudio/1/9/3/d/c45c-e3cc-4aab-b05c-505726ae3535.jpg",
          "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/extaudio/1/9/3/d/c45c-e3cc-4aab-b05c-505726ae3535.jpg",
          "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/extaudio/1/9/3/d/c45c-e3cc-4aab-b05c-505726ae3535.jpg",
          "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/extaudio/1/9/3/d/c45c-e3cc-4aab-b05c-505726ae3535.jpg",
          "1024wx1024h": "https://thumbnailer.mixcloud.com/unsafe/1024x1024/extaudio/1/9/3/d/c45c-e3cc-4aab-b05c-505726ae3535.jpg",
          "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/extaudio/1/9/3/d/c45c-e3cc-4aab-b05c-505726ae3535.jpg"
        },
        "repost_count": 0,
        "updated_time": "2016-04-07T03:34:40Z",
        "comment_count": 0
      },
      {
        "tags": [
          {
            "url": "https://www.mixcloud.com/discover/hardstyle/",
            "name": "Hardstyle",
            "key": "/discover/hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/hardcore/",
            "name": "Hardcore",
            "key": "/discover/hardcore/"
          }
        ],
        "play_count": 34,
        "user": {
          "url": "https://www.mixcloud.com/djadriano/",
          "username": "djadriano",
          "name": "DJ Adriano Fernandes",
          "key": "/djadriano/",
          "pictures": {
            "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg"
          }
        },
        "key": "/djadriano/hands-up-in-the-air-47/",
        "created_time": "2016-03-28T11:26:22Z",
        "audio_length": 3114,
        "slug": "hands-up-in-the-air-47",
        "favorite_count": 0,
        "listener_count": 1,
        "name": "DJ Adriano Fernandes - Hands Up In the Air 47",
        "url": "https://www.mixcloud.com/djadriano/hands-up-in-the-air-47/",
        "pictures": {
          "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/extaudio/3/6/a/6/9b02-f6c1-44ef-b958-85e3acb34130.jpg",
          "768wx768h": "https://thumbnailer.mixcloud.com/unsafe/768x768/extaudio/3/6/a/6/9b02-f6c1-44ef-b958-85e3acb34130.jpg",
          "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/extaudio/3/6/a/6/9b02-f6c1-44ef-b958-85e3acb34130.jpg",
          "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/3/6/a/6/9b02-f6c1-44ef-b958-85e3acb34130.jpg",
          "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/extaudio/3/6/a/6/9b02-f6c1-44ef-b958-85e3acb34130.jpg",
          "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/extaudio/3/6/a/6/9b02-f6c1-44ef-b958-85e3acb34130.jpg",
          "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/extaudio/3/6/a/6/9b02-f6c1-44ef-b958-85e3acb34130.jpg",
          "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/extaudio/3/6/a/6/9b02-f6c1-44ef-b958-85e3acb34130.jpg",
          "1024wx1024h": "https://thumbnailer.mixcloud.com/unsafe/1024x1024/extaudio/3/6/a/6/9b02-f6c1-44ef-b958-85e3acb34130.jpg",
          "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/extaudio/3/6/a/6/9b02-f6c1-44ef-b958-85e3acb34130.jpg"
        },
        "repost_count": 0,
        "updated_time": "2016-03-28T11:23:47Z",
        "comment_count": 0
      },
      {
        "tags": [
          {
            "url": "https://www.mixcloud.com/discover/hardstyle/",
            "name": "Hardstyle",
            "key": "/discover/hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/raw-hardstyle/",
            "name": "Raw Hardstyle",
            "key": "/discover/raw-hardstyle/"
          }
        ],
        "play_count": 8,
        "user": {
          "url": "https://www.mixcloud.com/djadriano/",
          "username": "djadriano",
          "name": "DJ Adriano Fernandes",
          "key": "/djadriano/",
          "pictures": {
            "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg"
          }
        },
        "key": "/djadriano/dj-adriano-fernandes-hands-up-in-the-air-46/",
        "created_time": "2016-03-21T03:25:34Z",
        "audio_length": 3169,
        "slug": "dj-adriano-fernandes-hands-up-in-the-air-46",
        "favorite_count": 0,
        "listener_count": 1,
        "name": "Dj Adriano Fernandes - Hands Up In The Air 46",
        "url": "https://www.mixcloud.com/djadriano/dj-adriano-fernandes-hands-up-in-the-air-46/",
        "pictures": {
          "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/extaudio/2/b/5/e/d925-b0fb-41ba-96e1-16b7aa50a110",
          "768wx768h": "https://thumbnailer.mixcloud.com/unsafe/768x768/extaudio/2/b/5/e/d925-b0fb-41ba-96e1-16b7aa50a110",
          "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/extaudio/2/b/5/e/d925-b0fb-41ba-96e1-16b7aa50a110",
          "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/2/b/5/e/d925-b0fb-41ba-96e1-16b7aa50a110",
          "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/extaudio/2/b/5/e/d925-b0fb-41ba-96e1-16b7aa50a110",
          "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/extaudio/2/b/5/e/d925-b0fb-41ba-96e1-16b7aa50a110",
          "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/extaudio/2/b/5/e/d925-b0fb-41ba-96e1-16b7aa50a110",
          "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/extaudio/2/b/5/e/d925-b0fb-41ba-96e1-16b7aa50a110",
          "1024wx1024h": "https://thumbnailer.mixcloud.com/unsafe/1024x1024/extaudio/2/b/5/e/d925-b0fb-41ba-96e1-16b7aa50a110",
          "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/extaudio/2/b/5/e/d925-b0fb-41ba-96e1-16b7aa50a110"
        },
        "repost_count": 0,
        "updated_time": "2016-03-21T14:05:21Z",
        "comment_count": 0
      },
      {
        "tags": [
          {
            "url": "https://www.mixcloud.com/discover/hardstyle/",
            "name": "Hardstyle",
            "key": "/discover/hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/hardcore/",
            "name": "Hardcore",
            "key": "/discover/hardcore/"
          },
          {
            "url": "https://www.mixcloud.com/discover/raw-hardstyle/",
            "name": "Raw Hardstyle",
            "key": "/discover/raw-hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/euphoric-hardstyle/",
            "name": "Euphoric Hardstyle",
            "key": "/discover/euphoric-hardstyle/"
          }
        ],
        "play_count": 12,
        "user": {
          "url": "https://www.mixcloud.com/djadriano/",
          "username": "djadriano",
          "name": "DJ Adriano Fernandes",
          "key": "/djadriano/",
          "pictures": {
            "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg"
          }
        },
        "key": "/djadriano/hands-up-in-the-air-45/",
        "created_time": "2016-03-06T14:39:40Z",
        "audio_length": 2541,
        "slug": "hands-up-in-the-air-45",
        "favorite_count": 0,
        "listener_count": 2,
        "name": "Hands Up In the Air 45",
        "url": "https://www.mixcloud.com/djadriano/hands-up-in-the-air-45/",
        "pictures": {
          "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/extaudio/9/5/e/7/96f1-d0f1-4c4f-981d-2f0f18879b67.jpg",
          "768wx768h": "https://thumbnailer.mixcloud.com/unsafe/768x768/extaudio/9/5/e/7/96f1-d0f1-4c4f-981d-2f0f18879b67.jpg",
          "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/extaudio/9/5/e/7/96f1-d0f1-4c4f-981d-2f0f18879b67.jpg",
          "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/9/5/e/7/96f1-d0f1-4c4f-981d-2f0f18879b67.jpg",
          "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/extaudio/9/5/e/7/96f1-d0f1-4c4f-981d-2f0f18879b67.jpg",
          "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/extaudio/9/5/e/7/96f1-d0f1-4c4f-981d-2f0f18879b67.jpg",
          "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/extaudio/9/5/e/7/96f1-d0f1-4c4f-981d-2f0f18879b67.jpg",
          "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/extaudio/9/5/e/7/96f1-d0f1-4c4f-981d-2f0f18879b67.jpg",
          "1024wx1024h": "https://thumbnailer.mixcloud.com/unsafe/1024x1024/extaudio/9/5/e/7/96f1-d0f1-4c4f-981d-2f0f18879b67.jpg",
          "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/extaudio/9/5/e/7/96f1-d0f1-4c4f-981d-2f0f18879b67.jpg"
        },
        "repost_count": 0,
        "updated_time": "2016-03-06T14:38:14Z",
        "comment_count": 0
      },
      {
        "tags": [
          {
            "url": "https://www.mixcloud.com/discover/hardstyle/",
            "name": "Hardstyle",
            "key": "/discover/hardstyle/"
          }
        ],
        "play_count": 11,
        "user": {
          "url": "https://www.mixcloud.com/djadriano/",
          "username": "djadriano",
          "name": "DJ Adriano Fernandes",
          "key": "/djadriano/",
          "pictures": {
            "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg"
          }
        },
        "key": "/djadriano/dj-adriano-fernandes-hands-up-in-the-air-44/",
        "created_time": "2016-02-26T13:14:26Z",
        "audio_length": 3045,
        "slug": "dj-adriano-fernandes-hands-up-in-the-air-44",
        "favorite_count": 1,
        "listener_count": 5,
        "name": "Dj Adriano Fernandes - Hands Up In The Air 44",
        "url": "https://www.mixcloud.com/djadriano/dj-adriano-fernandes-hands-up-in-the-air-44/",
        "pictures": {
          "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/extaudio/4/b/9/a/a2c6-9cb7-4752-964a-e017963f6ffa",
          "768wx768h": "https://thumbnailer.mixcloud.com/unsafe/768x768/extaudio/4/b/9/a/a2c6-9cb7-4752-964a-e017963f6ffa",
          "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/extaudio/4/b/9/a/a2c6-9cb7-4752-964a-e017963f6ffa",
          "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/4/b/9/a/a2c6-9cb7-4752-964a-e017963f6ffa",
          "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/extaudio/4/b/9/a/a2c6-9cb7-4752-964a-e017963f6ffa",
          "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/extaudio/4/b/9/a/a2c6-9cb7-4752-964a-e017963f6ffa",
          "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/extaudio/4/b/9/a/a2c6-9cb7-4752-964a-e017963f6ffa",
          "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/extaudio/4/b/9/a/a2c6-9cb7-4752-964a-e017963f6ffa",
          "1024wx1024h": "https://thumbnailer.mixcloud.com/unsafe/1024x1024/extaudio/4/b/9/a/a2c6-9cb7-4752-964a-e017963f6ffa",
          "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/extaudio/4/b/9/a/a2c6-9cb7-4752-964a-e017963f6ffa"
        },
        "repost_count": 0,
        "updated_time": "2016-02-26T14:46:10Z",
        "comment_count": 0
      },
      {
        "tags": [
          {
            "url": "https://www.mixcloud.com/discover/hardstyle/",
            "name": "Hardstyle",
            "key": "/discover/hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/raw-hardstyle/",
            "name": "Raw Hardstyle",
            "key": "/discover/raw-hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/hardcore/",
            "name": "Hardcore",
            "key": "/discover/hardcore/"
          }
        ],
        "play_count": 7,
        "user": {
          "url": "https://www.mixcloud.com/djadriano/",
          "username": "djadriano",
          "name": "DJ Adriano Fernandes",
          "key": "/djadriano/",
          "pictures": {
            "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg"
          }
        },
        "key": "/djadriano/hands-up-in-the-air-43/",
        "created_time": "2016-02-20T00:31:26Z",
        "audio_length": 3429,
        "slug": "hands-up-in-the-air-43",
        "favorite_count": 1,
        "listener_count": 1,
        "name": "Hands Up In the Air 43",
        "url": "https://www.mixcloud.com/djadriano/hands-up-in-the-air-43/",
        "pictures": {
          "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/extaudio/3/3/8/3/9821-c3aa-44c3-897f-b37220d5a3d7.jpg",
          "768wx768h": "https://thumbnailer.mixcloud.com/unsafe/768x768/extaudio/3/3/8/3/9821-c3aa-44c3-897f-b37220d5a3d7.jpg",
          "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/extaudio/3/3/8/3/9821-c3aa-44c3-897f-b37220d5a3d7.jpg",
          "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/3/3/8/3/9821-c3aa-44c3-897f-b37220d5a3d7.jpg",
          "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/extaudio/3/3/8/3/9821-c3aa-44c3-897f-b37220d5a3d7.jpg",
          "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/extaudio/3/3/8/3/9821-c3aa-44c3-897f-b37220d5a3d7.jpg",
          "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/extaudio/3/3/8/3/9821-c3aa-44c3-897f-b37220d5a3d7.jpg",
          "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/extaudio/3/3/8/3/9821-c3aa-44c3-897f-b37220d5a3d7.jpg",
          "1024wx1024h": "https://thumbnailer.mixcloud.com/unsafe/1024x1024/extaudio/3/3/8/3/9821-c3aa-44c3-897f-b37220d5a3d7.jpg",
          "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/extaudio/3/3/8/3/9821-c3aa-44c3-897f-b37220d5a3d7.jpg"
        },
        "repost_count": 0,
        "updated_time": "2016-02-20T00:30:04Z",
        "comment_count": 0
      },
      {
        "tags": [
          {
            "url": "https://www.mixcloud.com/discover/hardstyle/",
            "name": "Hardstyle",
            "key": "/discover/hardstyle/"
          },
          {
            "url": "https://www.mixcloud.com/discover/hardcore/",
            "name": "Hardcore",
            "key": "/discover/hardcore/"
          }
        ],
        "play_count": 8,
        "user": {
          "url": "https://www.mixcloud.com/djadriano/",
          "username": "djadriano",
          "name": "DJ Adriano Fernandes",
          "key": "/djadriano/",
          "pictures": {
            "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg",
            "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/profile/d/e/d/b/71fb-5a67-4994-9a94-4deb4ed733f4.jpg"
          }
        },
        "key": "/djadriano/hands-up-in-the-air-42/",
        "created_time": "2016-02-05T00:09:41Z",
        "audio_length": 3335,
        "slug": "hands-up-in-the-air-42",
        "favorite_count": 1,
        "listener_count": 1,
        "name": "Hands Up In the Air 42",
        "url": "https://www.mixcloud.com/djadriano/hands-up-in-the-air-42/",
        "pictures": {
          "medium": "https://thumbnailer.mixcloud.com/unsafe/100x100/extaudio/5/b/4/d/04b1-69ae-47c4-9423-72c79a67a2e4.jpg",
          "768wx768h": "https://thumbnailer.mixcloud.com/unsafe/768x768/extaudio/5/b/4/d/04b1-69ae-47c4-9423-72c79a67a2e4.jpg",
          "320wx320h": "https://thumbnailer.mixcloud.com/unsafe/320x320/extaudio/5/b/4/d/04b1-69ae-47c4-9423-72c79a67a2e4.jpg",
          "extra_large": "https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/5/b/4/d/04b1-69ae-47c4-9423-72c79a67a2e4.jpg",
          "large": "https://thumbnailer.mixcloud.com/unsafe/300x300/extaudio/5/b/4/d/04b1-69ae-47c4-9423-72c79a67a2e4.jpg",
          "640wx640h": "https://thumbnailer.mixcloud.com/unsafe/640x640/extaudio/5/b/4/d/04b1-69ae-47c4-9423-72c79a67a2e4.jpg",
          "medium_mobile": "https://thumbnailer.mixcloud.com/unsafe/80x80/extaudio/5/b/4/d/04b1-69ae-47c4-9423-72c79a67a2e4.jpg",
          "small": "https://thumbnailer.mixcloud.com/unsafe/25x25/extaudio/5/b/4/d/04b1-69ae-47c4-9423-72c79a67a2e4.jpg",
          "1024wx1024h": "https://thumbnailer.mixcloud.com/unsafe/1024x1024/extaudio/5/b/4/d/04b1-69ae-47c4-9423-72c79a67a2e4.jpg",
          "thumbnail": "https://thumbnailer.mixcloud.com/unsafe/50x50/extaudio/5/b/4/d/04b1-69ae-47c4-9423-72c79a67a2e4.jpg"
        },
        "repost_count": 0,
        "updated_time": "2016-02-05T00:09:20Z",
        "comment_count": 0
      }
    ],
    "name": "DJ Adriano Fernandes's Cloudcasts"
  }
  });

});

export default router;
