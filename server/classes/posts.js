import request from 'request-promise';
import cheerio from 'cheerio';
import {google}  from 'googleapis';
import Q       from 'q';

const youtube = google.youtube({
  version: 'v3',
  auth   : 'AIzaSyAdbILc-FIh3ufqQ8ywX6BRYjkTD8HMZpA'
});

class Posts {

  constructor() {
    this.arrPosts       = [];
    this.arrPostsVideos = [];
    this.currentPage    = 0;
  }

  getPosts( page = null ) {

    let paramPage = '';

    if( page ) {
      paramPage = '/page/' + page;
    }

    let url = 'https://www.hardstyle.com/hardstyle-releases/tracks' + paramPage;

    var options = {
      uri: url,
      transform: ( body ) => {
        return cheerio.load( body );
      }
    };

    return request( options )
            .then( this.storePosts.bind( this ) );

  }

  storePosts( $ ) {
    let posts = $( 'table.list tr' );
    let promises = [];

    let arrFoo = posts.filter((i, item) => {
        return item.attribs.class != undefined;
    });

    for( let i = 0; i < arrFoo.length; i++ ) {
      let deferred = Q.defer();

      let postsDataImage  = $( arrFoo[ i ] ).find('.image img')[ 0 ];
      let postsData       = $( arrFoo[ i ] ).find('.text-1 a')[ 0 ];
      let postsInfoData   = $( arrFoo[ i ] ).find('.text-2 a')[ 0 ];
      let postsDataLink   = postsData.attribs.href;
      let postsDataId     = null;

      if(postsDataLink) {
        postsDataId = postsDataLink.split('/').pop();
      }

      let artistName      = postsData.children[2] != undefined ? postsData.children[2] : postsData.children[1];
      let musicName       = postsData.children[0] || null;
      let labelName       = postsInfoData.children[2] || null;
      let musicImage      = postsDataImage.attribs.src ? postsDataImage.attribs.src.replace('48x48', '500x500') : null;
      let artistAndMusic  = artistName.children && musicName.children ? `${artistName.children[0].data} - ${musicName.children[0].data}` : null;
      let titleSearchGabba = artistAndMusic ? artistAndMusic.split(' ').join('+') : null;
      titleSearchGabba = titleSearchGabba.replace(`'`, '');

      let postData = {
        artist: artistName.children ? artistName.children[0].data : null,
        music: musicName.children ? musicName.children[0].data : null,
        label: labelName.children ? labelName.children[0].data : null,
        image: musicImage,
        id: postsDataId,
        shopping: postsData.attribs.href,
        gabbaLink: null
      };

      let promise = this.getGabbaLink( titleSearchGabba ).then(( $ ) => {

        let itemSearchList = $( 'div.views-row-first .ttl4reg' );

        if( itemSearchList ) {
          if( itemSearchList.length && itemSearchList[0].hasOwnProperty('attribs') ) {
            postData.gabbaLink = `http://1gabba.net${itemSearchList[0].attribs.href}`;
          }

          deferred.resolve( postData );

        } else {
          deferred.reject();
        }

      });

      promises.push( deferred.promise );

    }

    return Q
      .all(promises)
      .then((data) => {
        // remove musics duplicated on array
        this.arrPosts = data.reduce((field, e1) => {
          var matches = field.filter((e2) => {return e1.music == e2.music});
          if (matches.length == 0){
            field.push(e1);
          }
          return field;
        }, []);

        return this.arrPosts.filter(item => item.gabbaLink);
      });
  }

  getGabbaLink( title ) {
    var options = {
      uri: `http://1gabba.net/frontpage?title=${ title }`,
      transform: ( body ) => {
        return cheerio.load( body );
      }
    };

    return request( options );
  }

  getYoutubeVideos() {

    let promises = [];

    this.arrPosts.forEach((item, index) => {

      let deferred = Q.defer();

      let queryOptions = {
          'part'           : 'snippet',
          'maxResults'     : 1,
          'type'           : 'video',
          'videoDuration'  : 'any',
          'videoCategoryId': 10,
          'q'              : `"${item.artist} - ${item.music}"`
      };

      let promise = youtube.search.list(queryOptions, (err, data) => {
        if( err ) {
          deferred.reject(data)
        } else {
          if( data.items.length ) {
            if( data.items[0].snippet.title.includes(`${item.artist} - ${item.music}`) ) {
              deferred.resolve({ info: item, data: data });
            } else {
              deferred.resolve({ info: item, data: {items: []} })
            }
          } else {
            deferred.resolve({ info: item, data: data});
          }
        }
      });

      promises.push(deferred.promise);

    });

    return Q
      .all(promises)
      .then((data) => {
        let dataFiltered = data.filter((item) => {
          if( item.data.items.length ) return item;
        });

      return dataFiltered;
    });

  }

}

export default Posts;
