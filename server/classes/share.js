import request from 'request-promise';
import cheerio from 'cheerio';
import {google}  from 'googleapis';
import Q       from 'q';

const youtube = google.youtube({
  version: 'v3',
  auth   : 'AIzaSyAdbILc-FIh3ufqQ8ywX6BRYjkTD8HMZpA'
});

class Share {

  constructor() {}

  getVideoInfo( videoId ) {

    let deferred = Q.defer();

    let queryOptions = {
      'part'           : 'snippet',
      'maxResults'     : 1,
      'videoCategoryId': 10,
      'id'             : videoId
    };

    youtube.videos.list(queryOptions, (err, data) => {
      err ? deferred.reject(result) : deferred.resolve({ data: data });
    });

    return Q.all( deferred.promise );

  }

}

export default Share;
