import request from 'request-promise';
import Q       from 'q';

class Podcasts {
  constructor() {}

  getPodcasts() {

    let options = {
        uri    : 'https://api.mixcloud.com/djadriano/cloudcasts',
        headers: { 'User-Agent': 'Request-Promise' },
        json: true
    };

    return request( options );

  }
}

export default Podcasts;
